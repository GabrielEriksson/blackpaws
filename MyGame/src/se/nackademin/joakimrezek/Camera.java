package se.nackademin.joakimrezek;

import se.nackademin.joakimrezek.entity.Player;

/*
 * creating the camera to follow player
 */
public class Camera {

    private float  x = 0, y = 0; //initializing the coordinates
    private boolean stopMove = false;
    /*
     * making camera follow player
     */
    public void tick(Player player) {
    	if(player.getX() > (MyGame.WIDTH / 2) && !(player.getX() > 13360) && !stopMove) //camera follow only if player is on the right side of the screen
        x += ((-player.getX() + MyGame.WIDTH / 2) - x);						//and stops following the player when at 13360
    	
    	if(!(player.getY() < -2100) && !(player.getY() > 2600) && !stopMove)	//only following the player if player is between -2100 and 2600 in y-axel
    	y += ((-player.getY() + MyGame.HEIGHT / 2 + 210) - y);	
    }

    public float getX() { //returning the x
    	return x;
    }

    public float getY() { // returning the y
        return y;
    }
    
    public void setPosition(float x, float y){
    	this.x = x;
    	this.y = y;
    }
    
    public void setMovement( boolean stopMove){
    	this.stopMove = stopMove;
    }

}
package se.nackademin.joakimrezek;

import org.lwjgl.openal.AL;
import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

import se.nackademin.joakimrezek.states.Game;
import se.nackademin.joakimrezek.states.GameOver;
import se.nackademin.joakimrezek.states.HowToPlay;
import se.nackademin.joakimrezek.states.Menu;
import se.nackademin.joakimrezek.utils.ResourceLoader;

public class MyGame extends StateBasedGame{
	private static AppGameContainer app;
	public static final String gameName = "Black Paws";
	public static final int menu = 0;
	public static int howToPlay = 1;
	public static int game = 2;
	public static int gameOver = 3;

	
	public static int WIDTH = 1280, HEIGHT = 800;
	
	public MyGame(String gameName) {
		super(gameName);
		ResourceLoader.loadSounds();
		this.addState(new Menu(menu));
		this.addState(new HowToPlay(howToPlay));
		this.addState(new Game(game));
		this.addState(new GameOver(gameOver));
	}

	@Override
	public void initStatesList(GameContainer gc) throws SlickException {
		
		this.enterState(menu);
		
	}
	
	
	public static void main(String args[]) {
	
		try{
			app = new AppGameContainer(new MyGame(gameName));
			app.setDisplayMode(WIDTH, HEIGHT, false);
			app.setTargetFrameRate(60);
			app.setShowFPS(false);
			app.start();
			
		}catch(SlickException e){
			e.printStackTrace();
		}
		
	}
	
	 private static void cleanUp() {
	        AL.destroy(); 
	    }
	 
	 public static void exit(){
		 cleanUp();
		 System.exit(0);
	 }
	

}

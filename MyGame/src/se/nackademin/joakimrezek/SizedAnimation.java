package se.nackademin.joakimrezek;

import org.newdawn.slick.Animation;
import org.newdawn.slick.SpriteSheet;

/*
 * making my own animation class
 */

public class SizedAnimation extends Animation {
	private int sizeX, sizeY; //initializing sizes
	
	public SizedAnimation(SpriteSheet frames, int duration, int sizeX, int sizeY) { // constructor with super from Animation class from slick
		super(frames, duration);
		this.sizeX = sizeX;
		this.sizeY = sizeY;
	}

	public int getSizeX() { //returning size X
		return sizeX;
	}

	public void setSizeX(int sizeX) { //setting size X
		this.sizeX = sizeX;
	}

	public int getSizeY() { //returning size Y
		return sizeY;
	}

	public void setSizeY(int sizeY) { //setting size Y
		this.sizeY = sizeY;
	}

}

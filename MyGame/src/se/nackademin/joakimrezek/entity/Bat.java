package se.nackademin.joakimrezek.entity;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Shape;
import org.newdawn.slick.state.StateBasedGame;

import se.nackademin.joakimrezek.SizedAnimation;

/*
 * creating the bats
 */

public class Bat {
	
	private Player player;  //needs to know who player is
	private Shape rect, rectVision; //instances of shapes
	private int x, y; //instances of coordinates
	
	private boolean toTheLeft; // states to know where player is at
	private boolean isBelow;   //states to know where player is at
	private boolean isSeen;    //if seen, attack

	
	//making instances of spritesheets and animations
	private SpriteSheet batResting, batAttackLeft, batAttackRight;
	private SizedAnimation batRestingAnimation, batAttackRightAnimation, batAttackLeftAnimation;
	
	private SpriteSheet explosion;
	private SizedAnimation explosionAnimation;
	
	private SizedAnimation currentAnimation;

	public Bat(int x, int y, Player player){ //constructor
		this.x = x;
		this.y = y;
		this.player = player;
		
	}
	/*
	 * initializing all instances
	 */
	public void init(GameContainer gc, StateBasedGame sbg)throws SlickException {
		toTheLeft = false;
		isBelow = false;
		isSeen = false;
		
		rect = new Rectangle(x + 35, y, 50, 50);
		rectVision = new Rectangle(x - 185, y, 500, 500);
		
		
		batResting = new SpriteSheet("res/TheBatResting.png", 306, 300);
		batRestingAnimation = new SizedAnimation(batResting, 100, 120, 130);
		
		batAttackRight = new SpriteSheet("res/TheBatAttackRight.png", 307, 300);
		batAttackRightAnimation = new SizedAnimation(batAttackRight, 100, 120, 130);
		batAttackLeft = new SpriteSheet("res/TheBatAttackLeft.png", 307, 300);
		batAttackLeftAnimation = new SizedAnimation(batAttackLeft, 100, 120, 130);
		
		explosion = new SpriteSheet("res/Explosion50.png", 307, 300);
		explosionAnimation = new SizedAnimation(explosion, 130, 120, 130);
		
		currentAnimation = batRestingAnimation;

	}
	
	public void update(GameContainer gc, StateBasedGame sbg, int delta)throws SlickException {
		
		if(x < player.getX() + 50) toTheLeft = true; //is bat to the players left
		if(x > player.getX() - 50) toTheLeft = false; //is bat to the players right
		if(y < player.getY() - 50) isBelow = false; //is bat above the player
		if(y > player.getY() + 50) isBelow = true; //is bat below the player
		
		/*
		 * attacking
		 */
		if(isSeen){
			if(toTheLeft){
				currentAnimation =  batAttackRightAnimation;
				x += 2;
			} else {
				currentAnimation = batAttackLeftAnimation;	
				x -= 2;
			}
			if(isBelow){
				y -= 2;
			} else {
				y += 2;
			}
			
			/*
			 * making shapes follow bat
			 */
			rect.setX(x + 35); 
			if(isSeen) rect.setY(y + 35);
			else rect.setY(y);
			rectVision.setX(x - 185); rectVision.setY(y);
		}		
	}
	
	/*
	 * rendering currentAnimation
	 */
	public void render(GameContainer gc, StateBasedGame sbg, Graphics g)throws SlickException {		
		currentAnimation.draw(x, y, currentAnimation.getSizeX(), currentAnimation.getSizeY());
	}
	
	
	public void attack(boolean seesMe){ //setting state isSeen
		isSeen = seesMe;
	}
	
	public Shape getShape(){ //returning bat shape
		return rect;
	}
	
	public Shape getVisionShape(){ //returning vision shape
		return rectVision;
	}
	
	public float getX(){ //returning x
		return x;
	}
	
	public float getY(){ //returning y
		return y;
	}
	
	public void destroySelf() throws SlickException {
		currentAnimation = explosionAnimation;
		currentAnimation.stopAt(3);
	}

}

package se.nackademin.joakimrezek.entity;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Shape;
import org.newdawn.slick.state.StateBasedGame;

import se.nackademin.joakimrezek.SizedAnimation;

/*
 * creating the bubbahs
 */

public class Bubbah {
	
	/*
	 * making instances of sprites, animations, coordinates, walking distance, shapes and states
	 */
	private SpriteSheet bubbahWalkRight;
	private SpriteSheet bubbahWalkLeft;
	private SpriteSheet bubbahScaredRight;
	private SpriteSheet bubbahScaredLeft;
	private SpriteSheet bubbahAttackRight;
	private SpriteSheet bubbahAttackLeft;
	private SizedAnimation bubbahWalkRightAnimation;
	private SizedAnimation bubbahWalkLeftAnimation;
	private SizedAnimation bubbahScaredRightAnimation;
	private SizedAnimation bubbahScaredLeftAnimation;
	private SizedAnimation bubbahAttackRightAnimation;
	private SizedAnimation bubbahAttackLeftAnimation;
	
	private SpriteSheet explosion;
	private SizedAnimation explosionAnimation;
	
	private SizedAnimation currentAnimation;

	private float x;
	private float y;
	private float vX; //speed
	private int length; //walking distance
	int originalX; //starting position X
	private Shape rect;
	private Shape rectVision;
	private Shape attackShape;
	
	boolean isScared;
	boolean walkingRight;
	

	public Bubbah(int x, int y, int length){ //constructor
		this.x = x;
		this.y = y;
		this.length = length;
		
		originalX = x;
	}
	
	/*
	 * initializing all instances
	 */
	public void init(GameContainer gc, StateBasedGame sbg)throws SlickException {
		vX = 0.5f;
		isScared = false;
		walkingRight = true;
		
		rect = new Rectangle(x, y + 70, 100, 100);
		rectVision = new Rectangle(x, y + 70, 400, 100);
		attackShape = new Rectangle(x, y + 70, 2, 2);
		
		bubbahWalkRight = new SpriteSheet("res/BubbaWalk.png", 307, 300);
		bubbahWalkRightAnimation = new SizedAnimation(bubbahWalkRight, 100, 250, 230);
		bubbahWalkLeft = new SpriteSheet("res/BubbahWalkLeft.png", 307, 300);
		bubbahWalkLeftAnimation = new SizedAnimation(bubbahWalkLeft, 100, 250, 230);
		bubbahScaredRight = new SpriteSheet("res/BubbahScared.png", 307, 300);
		bubbahScaredRightAnimation = new SizedAnimation(bubbahScaredRight, 100, 250, 230);
		bubbahScaredLeft = new SpriteSheet("res/BubbahScaredLeft.png", 307, 300);
		bubbahScaredLeftAnimation = new SizedAnimation(bubbahScaredLeft, 100, 250, 230);
		bubbahAttackRight = new SpriteSheet("res/BubbahAttack.png", 307, 300);
		bubbahAttackRightAnimation = new SizedAnimation(bubbahAttackRight, 100, 250, 230);
		bubbahAttackLeft = new SpriteSheet("res/BubbahAttackLeft.png", 307, 300);
		bubbahAttackLeftAnimation = new SizedAnimation(bubbahAttackLeft, 100, 250, 230);
		
		explosion = new SpriteSheet("res/Explosion100.png", 307, 300);
		explosionAnimation = new SizedAnimation(explosion, 130, 120, 130);
		
		currentAnimation = bubbahWalkRightAnimation;
		
	}
		
	public void update(GameContainer gc, StateBasedGame sbg, int delta) throws SlickException {

		if(walkingRight){ //setting bubbah to walk right and shapes to follow
			x += vX;
			rectVision.setX(x + 100);
			attackShape.setX(x + 230); attackShape.setY(y + 170);
		}
		
		if(!walkingRight){ //setting bubbah to walk left and shapes to follow
			x-= vX;
			rectVision.setX(x - 175);
			attackShape.setX(x + 20); attackShape.setY(y + 170);
		}
		
		if(x >= (originalX + length)){ //making game to know if bubbah reaches turning point
			walkingRight = false;
			currentAnimation = bubbahWalkLeftAnimation;
		}
		if(x <= originalX){ // making game to know if bubbah reaches second turning point
			walkingRight = true;
			currentAnimation = bubbahWalkRightAnimation;
		}
		
		rect.setX(x + 80); //making hitbox shape to follow
		
		
	}
	
	/*
	 * rendering current animation
	 */
	public void render(GameContainer gc, StateBasedGame sbg, Graphics g)throws SlickException {
		currentAnimation.draw(x, y, currentAnimation.getSizeX(), currentAnimation.getSizeY());
	}
	
	/*
	 * bubbah suppose to be scared and jump before attacking
	 */
	public void reaction(){
		vX = 0;
		if(walkingRight){
			currentAnimation = bubbahScaredRightAnimation;
		}
		else {
			currentAnimation = bubbahScaredLeftAnimation;
		}
	}
	
	/*
	 * bubbah attacks
	 */
	public void attack(boolean seesMe){
		if(!seesMe) {
			vX = 0.5f; //increasing the speed and checking if right or left
			if(walkingRight)
			currentAnimation = bubbahWalkRightAnimation;
			else
				currentAnimation = bubbahWalkLeftAnimation;
			return;
		}
		else{
		vX = 2f; //else back to normal
		if(walkingRight)
			currentAnimation = bubbahAttackRightAnimation;
		
		else
			currentAnimation = bubbahAttackLeftAnimation;
		}
	}
	
	/*
	 * returning all shapes
	 */
	public Shape getShape(){
		return rect;
	}
	
	public Shape getVisionShape(){
		return rectVision;
	}
	
	public Shape getAttackShape(){
		return attackShape;
	}
	
	public void destroySelf() throws SlickException {
		currentAnimation = explosionAnimation;		
//		currentAnimation.stopAt(3);
	}

}

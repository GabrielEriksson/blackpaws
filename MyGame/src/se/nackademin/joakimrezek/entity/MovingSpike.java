package se.nackademin.joakimrezek.entity;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Shape;
import org.newdawn.slick.state.StateBasedGame;

import se.nackademin.joakimrezek.SizedAnimation;

/*
 * creating the moving spikes
 */

public class MovingSpike {
	
	/*
	 * instances of coordinates, states, direction, shape, sprites and animations
	 */
	private int x, y;
	private int trapDown;
	private String direction;
	private boolean movingSpike = false;
	private boolean visible = false;
	
	private SpriteSheet spikesUp;
	private SizedAnimation spikeAnimationUp;
	private SpriteSheet spikesLeft;
	private SizedAnimation spikeAnimationLeft;
	private SpriteSheet spikesRight;
	private SizedAnimation spikeAnimationRight;
	private SpriteSheet spikesDown;
	private SizedAnimation spikeAnimationDown;
	private SizedAnimation currentAnimation;
	
	private int seconds;
	private int milliseconds;
	
	private Shape rect;
		
	public MovingSpike(int x, int y, boolean movingSpike, String direction){ //constructor
		this.x = x;
		this.y = y;
		this.direction = direction;
		this.movingSpike = movingSpike;
	}
	
	/*
	 * initializing all instances
	 */
	public void init(GameContainer gc, StateBasedGame sbg)throws SlickException {
		seconds = 0;
		milliseconds = 80;
		
		spikesUp = new SpriteSheet("res/TrapAnimation.png", 307, 300);
		spikeAnimationUp = new SizedAnimation(spikesUp, 100, 120, 130);
		spikeAnimationUp.setPingPong(true);
		spikesDown = new SpriteSheet("res/TrapAnimationDown.png", 307, 300);
		spikeAnimationDown = new SizedAnimation(spikesDown, 100, 120, 130);
		spikeAnimationDown.setPingPong(true);
		spikesRight = new SpriteSheet("res/TrapAnimationRight.png", 307, 300);
		spikeAnimationRight = new SizedAnimation(spikesRight, 100, 120, 130);
		spikeAnimationRight.setPingPong(true);
		spikesLeft = new SpriteSheet("res/TrapAnimationLeft.png", 307, 300);
		spikeAnimationLeft = new SizedAnimation(spikesLeft, 100, 120, 130);
		spikeAnimationLeft.setPingPong(true);
		
		rect = new Rectangle(x + 20, y, 75, 40);
		trapDown = y + 130;
		
	}
	/*
	 * setting the direction
	 */
	public void update(GameContainer gc, StateBasedGame sbg, int delta)throws SlickException {
		if(direction.equals("up") && movingSpike){
			currentAnimation = spikeAnimationUp;
		}
		if(direction.equals("down") && movingSpike){
			currentAnimation = spikeAnimationDown;
		}
		if(direction.equals("left") && movingSpike){
			currentAnimation = spikeAnimationLeft;
		}
		if(direction.equals("right") && movingSpike){
			currentAnimation = spikeAnimationRight;
		}
		
		milliseconds--;
		if(milliseconds <= 5 && milliseconds >= 0 ){
			visible = true;
		} else {
			visible = false;
		}
		
		if(milliseconds == 0){
			seconds--;
			milliseconds = 240;
		}
		
		
		if(seconds == -1){
			seconds = 59;
		}
		
		if(visible){
			rect.setY(rect.getY() - 10);
		} else {
			rect.setY(trapDown);
		}
	}
	
	/*
	 * rendering the current animation
	 */
	public void render(GameContainer gc, StateBasedGame sbg, Graphics g)throws SlickException {
		currentAnimation.draw(x, y, currentAnimation.getSizeX(), currentAnimation.getSizeY());
	}

	public Shape getShape(){ //returning the shape
		
		return rect;
	}

}

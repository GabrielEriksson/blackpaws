package se.nackademin.joakimrezek.entity;

import java.util.HashSet;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;
import org.newdawn.slick.geom.Circle;
import org.newdawn.slick.geom.Shape;
import org.newdawn.slick.state.StateBasedGame;

import se.nackademin.joakimrezek.Camera;
import se.nackademin.joakimrezek.SizedAnimation;
import se.nackademin.joakimrezek.libs.Reference;
import se.nackademin.joakimrezek.states.Game;
import se.nackademin.joakimrezek.utils.AudioPlayer;
import se.nackademin.joakimrezek.world.World;


public class Player {

	/*
	 * instances of coordinates, speed, calculations, booleanStates, sprites, animations, shapes, world and camera
	 */
	private float startPosX;
	private float startPosY;
	private float vX;
	private float vY;

	private int points;
	private int lives;

	HashSet<Rings> rings;

	private static float gravity;
	private boolean flying;
	private boolean inSpin;
	private boolean isDown;
	private boolean inSpeedRoll;
	private boolean runningMode;
	private boolean inSpeedRightMode;
	private boolean inSpeedLeftMode;
	private boolean fastBreak;
	private boolean rightOrLeft;
	private boolean losingLife;
	private boolean dying;
	private boolean trampoline;
	private boolean pushingLeft;
	private boolean pushingRight;
	private boolean hasPlayed;
	private boolean gameOver;
	private boolean levelComplete;
	private boolean ringsCounted;
	private boolean done;
	
	private boolean levelCompleteSoundPlayed;

	private int inLoop;
	private int inLoop2;
	private int inLoop3;
	private int inLoop4;
	private int currentStep;

	private SpriteSheet bpSittingRight;
	private SizedAnimation bpSittingRightAnimation;
	private SpriteSheet bpSittingLeft;
	private SizedAnimation bpSittingLeftAnimation;

	private SpriteSheet bpStanceLeft;
	private SizedAnimation bpStanceLeftAnimation;
	private SpriteSheet bpStanceRight;
	private SizedAnimation bpStanceRightAnimation;

	private SpriteSheet bpRunningLeft;
	private SizedAnimation bpRunningLeftAnimation;
	private SpriteSheet bpRunningRight;
	private SizedAnimation bpRunningRightAnimation;

	private SpriteSheet bpJumpRollLeft;
	private SizedAnimation bpJumpRollLeftAnimation;
	private SpriteSheet bpJumpRollRight;
	private SizedAnimation bpJumpRollRightAnimation;

	private SpriteSheet bpRunningFastLeft;
	private SizedAnimation bpRunningFastLeftAnimation;
	private SpriteSheet bpRunningFastRight;
	private SizedAnimation bpRunningFastRightAnimation;

	private SpriteSheet bpSpinChargeLeft;
	private SizedAnimation bpSpinChargeLeftAnimation;
	private SpriteSheet bpSpinChargeRight;
	private SizedAnimation bpSpinChargeRightAnimation;

	private SpriteSheet bpDyingLeft;
	private SizedAnimation bpDyingLeftAnimation;
	private SpriteSheet bpDyingRight;
	private SizedAnimation bpDyingRightAnimation;

	private SpriteSheet bpStopsLeft;
	private SizedAnimation bpStopsLeftAnimation;
	private SpriteSheet bpStopsRight;
	private SizedAnimation bpStopsRightAnimation;

	private SpriteSheet bpTrampolineLeft;
	private SizedAnimation bpTrampolineLeftAnimation;
	private SpriteSheet bpTrampolineRight;
	private SizedAnimation bpTrampolineRightAnimation;

	private SpriteSheet bpTrampolineDownLeft;
	private SizedAnimation bpTrampolineDownLeftAnimation;
	private SpriteSheet bpTrampolineDownRight;
	private SizedAnimation bpTrampolineDownRightAnimation;

	private SpriteSheet bpWallPushLeft;
	private SizedAnimation bpWallPushLeftAnimation;
	private SpriteSheet bpWallPushRight;
	private SizedAnimation bpWallPushRightAnimation;

	private SpriteSheet bpDies;
	private SizedAnimation bpDiesAnimation;


	private SizedAnimation currentAnimation;
	private Shape playerCircle;

	private World level;
	private Camera camera;

	public Player(World level, Camera camera){ //constructor
		this.level = level;
		this.camera = camera;
	}

	//init method
	public void init(GameContainer gc, StateBasedGame sbg)throws SlickException {
		startPosX = 200;
		startPosY = 600;
		vX = 0;
		vY = 0;

		points = 0;
		lives = 3;

		rings = new HashSet<Rings>();

		gravity = 9.81f;
		flying = true;
		inSpin = false;
		isDown = false;
		inSpeedRoll = false;
		runningMode = false;
		inSpeedRightMode = false;
		inSpeedLeftMode = false;
		fastBreak = false;
		rightOrLeft = true;
		losingLife = false;
		dying = false;
		trampoline = false;
		pushingLeft = false;
		pushingRight = false;
		hasPlayed = false;
		gameOver = false;
		levelComplete = false;
		ringsCounted = false;
		done = false;
		
		levelCompleteSoundPlayed = false;

		inLoop = -1;
		inLoop2 = -1;
		inLoop3 = -1;
		inLoop4 = 1;
		currentStep = 1;
		
		bpSittingRight = new SpriteSheet("res/bpSittingRight.png", 305, 300);
		bpSittingRightAnimation = new SizedAnimation(bpSittingRight, 1200, 120, 130);
		bpSittingLeft = new SpriteSheet("res/bpSittingLeft.png", 305, 300);
		bpSittingLeftAnimation = new SizedAnimation(bpSittingLeft, 1200, 120, 130);

		bpStanceLeft = new SpriteSheet("res/bpStance.png", 144, 300);
		bpStanceLeftAnimation = new SizedAnimation(bpStanceLeft, 150, 90, 130);
		bpStanceLeftAnimation.setPingPong(true);
		bpStanceRight = new SpriteSheet("res/bpStanceRight.png", 144, 300);
		bpStanceRightAnimation = new SizedAnimation(bpStanceRight, 150, 90, 130);
		bpStanceRightAnimation.setPingPong(true);

		bpRunningLeft = new SpriteSheet("res/bpRunningLeft.png", 305, 300);
		bpRunningLeftAnimation = new SizedAnimation(bpRunningLeft,100, 120, 130);
		bpRunningLeftAnimation.setPingPong(true);
		bpRunningRight = new SpriteSheet("res/bpRunningRight.png", 305, 300);
		bpRunningRightAnimation = new SizedAnimation(bpRunningRight,100, 120, 130);
		bpRunningRightAnimation.setPingPong(true);

		bpJumpRollLeft = new SpriteSheet("res/JumpnRollLeft.png", 305, 300);
		bpJumpRollLeftAnimation = new SizedAnimation(bpJumpRollLeft,100, 120, 130);
		bpJumpRollRight = new SpriteSheet("res/JumpnRollRight.png", 305, 300);
		bpJumpRollRightAnimation = new SizedAnimation(bpJumpRollRight,100, 120, 130);

		bpRunningFastLeft = new SpriteSheet("res/bpRunningFastLeft.png", 308, 300);
		bpRunningFastLeftAnimation = new SizedAnimation(bpRunningFastLeft,100, 120, 130);
		bpRunningFastRight = new SpriteSheet("res/bpRunningFastRight.png", 306, 300);
		bpRunningFastRightAnimation = new SizedAnimation(bpRunningFastRight,100, 120, 130);

		bpSpinChargeLeft = new SpriteSheet("res/SpinChargeLeft.png",305, 300);
		bpSpinChargeLeftAnimation = new SizedAnimation(bpSpinChargeLeft,100, 120, 130);
		bpSpinChargeRight = new SpriteSheet("res/SpinChargeRight.png",305,300);
		bpSpinChargeRightAnimation = new SizedAnimation(bpSpinChargeRight,100, 120, 130);

		bpDyingLeft = new SpriteSheet("res/bpDyingLeft.png",305,300);
		bpDyingLeftAnimation = new SizedAnimation(bpDyingLeft,100,120,130);
		bpDyingRight = new SpriteSheet("res/bpDyingRight.png",305,300);
		bpDyingRightAnimation = new SizedAnimation(bpDyingRight,100,120,130);

		bpStopsLeft = new SpriteSheet("res/bpStopsLeft.png",305,300);
		bpStopsLeftAnimation = new SizedAnimation(bpStopsLeft,100,160,130);
		bpStopsRight = new SpriteSheet("res/bpStopsRight.png",305,300);
		bpStopsRightAnimation = new SizedAnimation(bpStopsRight,100,160,130);

		bpTrampolineLeft = new SpriteSheet("res/bpTrampolineLeft.png",305,300);
		bpTrampolineLeftAnimation = new SizedAnimation(bpTrampolineLeft,100,160,130);
		bpTrampolineRight = new SpriteSheet("res/bpTrampolineRight.png",305,300);
		bpTrampolineRightAnimation = new SizedAnimation(bpTrampolineRight,100,160,130);

		bpTrampolineDownLeft = new SpriteSheet("res/bpTrampolinDownLeft.png",305,300);
		bpTrampolineDownLeftAnimation = new SizedAnimation(bpTrampolineDownLeft,100,160,130);
		bpTrampolineDownRight = new SpriteSheet("res/bpTrampolineDownRight.png",305,300);
		bpTrampolineDownRightAnimation = new SizedAnimation(bpTrampolineDownRight,100,160,130);

		bpWallPushLeft = new SpriteSheet("res/closeToWallLeft.png",304,300);
		bpWallPushLeftAnimation = new SizedAnimation(bpWallPushLeft,100,160,130);
		bpWallPushRight = new SpriteSheet("res/closeToWallRight.png",305,300);
		bpWallPushRightAnimation = new SizedAnimation(bpWallPushRight,200,160,130);

		bpDies = new SpriteSheet("res/bpDying.png", 304, 300);
		bpDiesAnimation = new SizedAnimation(bpDies, 500, 230, 200);

		currentAnimation = bpJumpRollRightAnimation;

	}

	//checking collisions, checking positions, changing booleans according to whatever happens, playing soundFX,
	public void update(GameContainer gc, StateBasedGame sbg, int delta)throws SlickException {
		if(startPosX < 100 && trampoline){
			startPosX = 100;
			vX = 0;
		}
		if(startPosX < 50){
			startPosX = 50;
			vX = 0;
			pushingLeft = true;
			currentAnimation = bpWallPushLeftAnimation;
		}
		if(startPosX > 13930){
			startPosX = 13930;
			vX = 0;
			pushingRight = true;
			currentAnimation = bpWallPushRightAnimation;
		}

		if(startPosY > 2700 && !dying) dies();
		if(fastBreak && vX > -0.1 && vX < 0.1) fastBreak = false;
		
		if (flying) {
			vY += gravity/10;
		}

		startPosY += vY;
		startPosX += vX;
		
		if(vX > 3){
			inSpeedRightMode = true;
			rightOrLeft = true;
		}
		if(vX < -3){
			inSpeedLeftMode = true;
			rightOrLeft = false;
		}
		if(vX > -3 && vX < 3){
			inSpeedRightMode = false;
			inSpeedLeftMode = false;
		}	
		
		if(flying && !trampoline) jump();

		if(trampoline && vY > 0 && rightOrLeft){
			currentAnimation = bpTrampolineDownRightAnimation;
		}
		if(trampoline && vY > 0 && !rightOrLeft){
			currentAnimation = bpTrampolineDownLeftAnimation;
		}
		if(vX == 0) hasPlayed = false;

		level.update(gc, sbg, delta);
		playerCircle = new Circle(startPosX, startPosY, 30, 30);
		playerCircle.setY(playerCircle.getY() + vY);
		playerCircle.setX(playerCircle.getX() + vX);


		if(level.collidesWithGround(playerCircle) && !dying){
			playerCircle.setY(playerCircle.getY() - vY);
			playerCircle.setX(playerCircle.getX() - vX);
			vY = 0;
			trampoline = false;
			flying = false;	

			if(losingLife){
				vX = 0;
				losingLife = false;
			}
		}
		else{
			flying = true;
		}
		
		if(level.collidesWithSRight(playerCircle) || level.collidesWithSRight(playerCircle) && level.collidesWithWall(playerCircle)){
			inSpeedRoll = true;
			trampoline = false;
			flying = false;	
			playerCircle.setY(playerCircle.getY() - vY);
			playerCircle.setX(playerCircle.getX() - vX);
			vY = 0;
			
			this.roll();
			vX += 0.9;
		}
		
		if(level.collidesWithSLeft(playerCircle) || level.collidesWithSRight(playerCircle) && level.collidesWithWall(playerCircle)){
			inSpeedRoll = true;
			trampoline = false;
			flying = false;	
			playerCircle.setY(playerCircle.getY() - vY);
			playerCircle.setX(playerCircle.getX() - vX);
			vY = 0;
			
			this.roll();
			vX -= 0.9;
		}

		if(level.collidesWithRoof(playerCircle) && !dying){
			playerCircle.setY(playerCircle.getY() - vY);
			playerCircle.setX(playerCircle.getX() - vX);
			vY = 0;
		}

		if(level.collidesWithWall(this.getRight()) && !dying || level.collidesWithWall(playerCircle) && !dying || level.collidesWithWall(this.getLeft()) && !dying){
			playerCircle.setY(playerCircle.getY() - vY);
			playerCircle.setX(playerCircle.getX() - vX);
			vX = 0;
		}


		if(level.collidesWithHillRight(playerCircle) && !dying){
			trampoline = false;
			flying = false;
			if(inSpeedRoll && !inSpin) vX -= 0.4;
			
			if(vX >= 0){
				playerCircle.setY(playerCircle.getY() - vY);
				playerCircle.setX(playerCircle.getX() - vX);
				startPosY -= 0.1 + vX;
				vY = 0;		
			}
			if(vX <= 0){
				playerCircle.setY(playerCircle.getY() - vY);
				playerCircle.setX(playerCircle.getX() - vX);
				startPosY -= 0.1 - vX;
						if(fastBreak){
							playerCircle.setY(playerCircle.getY() - vY);
							playerCircle.setX(playerCircle.getX() - vX);
							vY = 0;
						}	
			}
		}
		
		
		if(level.collidesWithHillLeft(playerCircle) && !dying){
			trampoline = false;
			flying = false;
			if(inSpeedRoll && !inSpin) vX += 0.4;
			
			if(vX >= 0){
				playerCircle.setY(playerCircle.getY() - vY);
				playerCircle.setX(playerCircle.getX() - vX);
				startPosY -= 0.1 + vX;
				if(fastBreak){
					playerCircle.setY(playerCircle.getY() - vY);
					playerCircle.setX(playerCircle.getX() - vX);
					vY = 0;
				}
			}	
			if(vX <= 0){
				playerCircle.setY(playerCircle.getY() - vY);
				playerCircle.setX(playerCircle.getX() - vX);
				startPosY -= 0.1 - vX;
				vY = 0;
				}
		}
		
		if(level.collidesWithEnd(playerCircle)){
			levelComplete = true;
			if(!levelCompleteSoundPlayed){ 
				AudioPlayer.playSound(Reference.SOUND_LEVELCOMPLETE);
				levelCompleteSoundPlayed = true;
			}
			if(levelComplete){
				camera.setMovement(true);
				AudioPlayer.getMusic(Reference.MUSIC_GAMEMUSIC).fade(500, 0, true);
			}

		}


		if(level.collidesWithBouncer(playerCircle) && !dying){
			vY = -33;
			currentAnimation = bpJumpRollRightAnimation;
			AudioPlayer.playSound(Reference.SOUND_BOUNCER);
		}
		if(level.collidesWithBounceR(playerCircle) && !dying){
			trampoline = true;
			vY = -40;
			vX = 30;
			currentAnimation = bpTrampolineRightAnimation;
			AudioPlayer.playSound(Reference.SOUND_BOUNCER);
		}
		if(level.collidesWithBounceL(playerCircle) && !dying){
			trampoline = true;
			vY = -40;
			vX = -30;
			currentAnimation = bpTrampolineLeftAnimation;
			AudioPlayer.playSound(Reference.SOUND_BOUNCER);
		}
		if(level.collidesWithBounceHard(playerCircle) && !dying){
			trampoline = true;
			vY = -30;
			vX = -60;
			currentAnimation = bpTrampolineLeftAnimation;
			AudioPlayer.playSound(Reference.SOUND_BOUNCER);
		}
		if(level.collidesWithBounceHardL(playerCircle) && !dying){
			vX = -60;
			AudioPlayer.playSound(Reference.SOUND_BOUNCER);
		}

		if(level.collidesWithFallingObjects(playerCircle) && !dying){
			playerCircle.setY(playerCircle.getY() - vY);
			playerCircle.setX(playerCircle.getX() - vX);
			vY = 0;
			trampoline = false;
			flying = false;
		}

		if(level.collidesWithSideLift(playerCircle) && level.getRightEnd() && !dying){
			vY = 0;
			startPosX -= 2;
			trampoline = false;
			flying = false;
		}
		if(level.collidesWithSideLift(playerCircle) && !level.getRightEnd() && !dying){
			vY = 0;
			startPosX += 2;
			trampoline = false;
			flying = false;
		}
		if(level.collidesWithVerticalLift(playerCircle) && level.getUpEnd() && !dying){
			vY = 0;
			startPosY += 2;
			trampoline = false;
			flying = false;
		}

		if(level.collidesWithVerticalLift(playerCircle) && !level.getUpEnd() && !dying){
			vY = 0;
			startPosY -= 2;
			trampoline = false;
			flying = false;
		}

		if(level.collidesWithDiagonalLift(playerCircle) && level.getDownLeftEnd() && !dying){
			vY = 0;
			startPosY -= 2;
			startPosX += 1;
			trampoline = false;
			flying = false;
		}
		if(level.collidesWithDiagonalLift(playerCircle) && !level.getDownLeftEnd() && !dying){
			vY = 0;
			startPosY += 2;
			startPosX -= 1;
			trampoline = false;
			flying = false;
		}

		if(!dying) addRing(level.collidesWithRing(playerCircle));

		if(level.collidesWithBubbah(playerCircle) && !dying && !Game.DONE){
			if(flying && !trampoline){
				AudioPlayer.playSound(Reference.SOUND_JUMPONENEMY);
				vY = -15;
				points += 100;
			}

			else if(!flying || trampoline){
				losesLife();
			}
		}

		if(level.collidesWithBat(playerCircle) && !dying && !Game.DONE){
			if(flying && !trampoline){
				AudioPlayer.playSound(Reference.SOUND_JUMPONENEMY);
				vY = -15;
				points += 50;
			}

			else if(!flying || trampoline){
				losesLife();
			}
		}

		level.bubbahSeesMe(playerCircle);
		level.batSeesMe(playerCircle);	

		if(level.collidesWithSpike(playerCircle) && !dying){
			AudioPlayer.playSound(Reference.SOUND_NAILSTING);
			losesLife();
		}
		if(level.collidesWithMovingSpike(playerCircle) && !dying && !Game.DONE){
			AudioPlayer.playSound(Reference.SOUND_NAILSTING);
			losesLife();
		}

		if(dying && startPosY > 2800){
			restart();
		}
		if(lives == 0) gameOver = true;

		if(getSpeedX() > 10 && inLoop == -1 && level.getInLoop(playerCircle) && inSpeedRoll){
			inLoop = 0;
		}
		if(getSpeedX() < -10 && inLoop == 1 && level.getInLoop(playerCircle) && inSpeedRoll){
			inLoop = 0;
		}
		if (inLoop == 0) {
			flying = false;
			inLoop = World.placePlayerInLoop(5100.0, 10.0, 170, this, currentStep++, getSpeedX() < 0);

			if (inLoop != 0) {
				currentStep = 1;
			}
		}
		if(getSpeedX() > 10 && inLoop2 == -1 && level.getInLoop2(playerCircle) && inSpeedRoll){
			inLoop2 = 0;
		}
		if(getSpeedX() < -10 && inLoop2 == 1 && level.getInLoop2(playerCircle) && inSpeedRoll){
			inLoop2 = 0;
		}
		if (inLoop2 == 0) {
			flying = false;
			inLoop2 = World.placePlayerInLoop(9748.0, -1060.0, 170, this, currentStep++, getSpeedX() < 0);

			if (inLoop2 != 0) {
				currentStep = 1;
			}
		}
		if(getSpeedX() > 10 && inLoop3 == -1 && level.getInLoop3(playerCircle) && inSpeedRoll){
			inLoop3 = 0;
		}
		if(getSpeedX() < -10 && inLoop3 == 1 && level.getInLoop3(playerCircle) && inSpeedRoll){
			inLoop3 = 0;
		}
		if (inLoop3 == 0) {
			flying = false;
			inLoop3 = World.placePlayerInLoop(10480.0, -592.0, 170, this, currentStep++, getSpeedX() < 0);

			if (inLoop3 != 0) {
				currentStep = 1;
			}
		}
		if(getSpeedX() > 10 && inLoop4 == -1 && level.getInLoop4(playerCircle) && inSpeedRoll){
			inLoop4 = 0;
		}
		if(getSpeedX() < -10 && inLoop4 == 1 && level.getInLoop4(playerCircle) && inSpeedRoll){
			inLoop4 = 0;
		}
		if (inLoop4 == 0) {
			flying = false;
			inLoop4 = World.placePlayerInLoop(9159.0, 1668.0, 170, this, currentStep++, getSpeedX() < 0);

			if (inLoop4 != 0) {
				currentStep = 1;
			}
		}
		
	}
	
	/*
	 * rendering the current animation which is constantly changing by inputs from keyboard
	 */
	public void render(GameContainer gc, StateBasedGame sbg, Graphics g)throws SlickException {
		currentAnimation.draw(playerCircle.getX() - 27, playerCircle.getY() -70,
				currentAnimation.getSizeX(), currentAnimation.getSizeY());

	}

	/*
	 * sitting method
	 */
	public void sit(){
		if(inSpeedRoll || runningMode){
			return;
		}
		if(inSpin){
			if(rightOrLeft){
				currentAnimation = bpSpinChargeRightAnimation;
			}
			else{
				currentAnimation = bpSpinChargeLeftAnimation;
			}
		}
		else{
			if(rightOrLeft){
				currentAnimation = bpSittingRightAnimation;
			}
			else{
				currentAnimation = bpSittingLeftAnimation;
			}
		}
	}
	
	/*
	 * spin charge method
	 */
	public void spinCharge(){
		if(!inSpin) return;
		if(inSpin){
			if(rightOrLeft)currentAnimation = bpSpinChargeRightAnimation;
			else currentAnimation = bpSpinChargeLeftAnimation;

		}
		if(!isDown && !runningMode){
			inSpin = false;
			if(level.collidesWithWall(playerCircle)) vX = 0;
			if(rightOrLeft){
				currentAnimation = bpJumpRollRightAnimation;
				vX = +40;
				AudioPlayer.playSound(Reference.SOUND_SPINCHARGERELEASED);
			}
			else{
				currentAnimation = bpJumpRollLeftAnimation;
				vX = -40;
				AudioPlayer.playSound(Reference.SOUND_SPINCHARGERELEASED);
			}
		}
	}

	/*
	 * running method
	 */
	public void runRight(){
		pushingLeft = false;
		if(level.collidesWithWall(this.getRight()) && !flying){
			pushingRight = true;
			currentAnimation = bpWallPushRightAnimation;
			return;
		}

		if(inSpeedRoll){
			this.breakX();
			return;
		}
		if (flying){
			vX += 0.2;
		}
		else{
			vX += 0.2;
			if (vX > 15 && !pushingLeft && !pushingRight){
				currentAnimation = bpRunningFastRightAnimation;
			}
			else if(vX < 15 && vX > 0 && !pushingLeft && !pushingRight){
				currentAnimation = bpRunningRightAnimation;
			}
		}
	}
	
	/*
	 * running method
	 */
	public void runLeft(){
		pushingRight = false;
		if(level.collidesWithWall(this.getLeft()) && !flying){
			currentAnimation = bpWallPushLeftAnimation;
			return;
		}

		if(inSpeedRoll){
			this.breakX();
			return;
		}
		if (flying){
			vX -= 0.2;
		}
		else{
			vX -= 0.2;
			if (vX < -15 && !pushingLeft && !pushingRight){
				currentAnimation = bpRunningFastLeftAnimation;
			}
			else if(vX > -15 && vX < 0 && !pushingLeft && !pushingRight){
				currentAnimation = bpRunningLeftAnimation;
			}
		}
	}

	/*
	 * rolling method
	 */
	public void roll(){
		if(!inSpeedRoll && !runningMode){
			return;
		}
		if(!hasPlayed){
			AudioPlayer.playSound(Reference.SOUND_SPINCHARGE);
			hasPlayed = true;
		}
		if(rightOrLeft && inSpeedRoll && !runningMode){
			currentAnimation = bpJumpRollRightAnimation;
		}
		else if(!rightOrLeft && inSpeedRoll && !runningMode){
			currentAnimation = bpJumpRollLeftAnimation;
		}
	}
	
	/*
	 * jumping method
	 */
	public void jump(){
		if (flying || inSpin) {
			return;
		}
		AudioPlayer.playSound(Reference.SOUND_JUMP);

		if(rightOrLeft){
			currentAnimation = bpJumpRollRightAnimation;
			flying = true;
			vY = -30;
		}
		else{
			currentAnimation = bpJumpRollLeftAnimation;
			flying = true;
			vY = -30;
		}
	}

	/*
	 * losing life method
	 */
	public void losesLife(){
		losingLife = true;
		flying = true;
		if(rings.size() == 1){
			dies();
		}

		else if(rings.size() > 1){
			if(rightOrLeft){
				vY = -15;
				vX = -15;
				currentAnimation = bpDyingLeftAnimation;
				rings.removeAll(rings);
				AudioPlayer.playSound(Reference.SOUND_RINGSLOST);
			}

			else if(!rightOrLeft){
				vY = -15;
				vX = +15;
				currentAnimation = bpDyingRightAnimation;
				rings.removeAll(rings);
				AudioPlayer.playSound(Reference.SOUND_RINGSLOST);
			}

		}

	}

	/*
	 * Dying method
	 */
	public void dies(){
		currentAnimation = bpDiesAnimation;
		vX = 0;
		vY = - 40;
		dying = true;
		trampoline = false;
		AudioPlayer.playSound(Reference.SOUND_DIES);
	}

	/*
	 * if I die, restart method
	 */
	public void restart(){
		currentAnimation = bpJumpRollRightAnimation;
		dying = false;
		startPosX = 660;
		startPosY = 500;
		vX = 0;
		vY = 0;
		camera.setPosition(0, 0);
		lives--;	
	}

	/*
	 * "runing so therefor I break" method
	 */
	public void stop(){
		if(flying || inSpin){
			return;
		}
		if(!hasPlayed){
			AudioPlayer.playSound(Reference.SOUND_BREAKS);
			hasPlayed = true;
		}

		if(inSpeedRightMode && !inSpeedLeftMode){
			fastBreak = true;
			currentAnimation = bpStopsRightAnimation;
			vX -=0.5;
		}
		else if(inSpeedLeftMode && !inSpeedRightMode) {
			fastBreak = true;
			currentAnimation = bpStopsLeftAnimation;
			vX +=0.5;
		}
	}	
	
	/*
	 * "jump on a bouncer" method
	 */
	public void trampolineJump(){
		if(losingLife)return;


		trampoline = true;
		if(rightOrLeft){
			vY = -40;
			currentAnimation = bpTrampolineRightAnimation;

		}
		else{
			currentAnimation = bpTrampolineLeftAnimation;
			vY = -40;
		}
	}
	
	/*
	 * falling method
	 */
	public void fall(){
		if(rightOrLeft) currentAnimation = bpTrampolineDownRightAnimation;
		else currentAnimation = bpTrampolineDownLeftAnimation;
	}


	/*
	 * "no inputs from keyboard" method
	 */
	public void breakX() {	
		fastBreak = false;
		if(!inSpeedRoll) vY = 0;
		
		
		if(level.collidesWithWall(this.getRight())) vX = -3;
		if(level.collidesWithWall(this.getLeft())) vX = +3;
		if(pushingRight && !pushingLeft){
			pushingRight = false;
			vX = -3;
		}
		if(pushingLeft && !pushingRight){
			pushingLeft = false;
			vX = +3;
		}

		if(vX > 5){
			inSpeedRightMode =true;
			if(fastBreak == true){
				currentAnimation = bpStopsRightAnimation;
			}
		}
		else if(vX < -5){
			inSpeedLeftMode =true;
			if(fastBreak == true){
				currentAnimation = bpStopsLeftAnimation;
			}
		}
		if(vX > 10){
			runningMode = true;
			currentAnimation = bpRunningFastRightAnimation;
			if(inSpeedRoll){
				currentAnimation = bpJumpRollRightAnimation;
			}
		}
		else if(vX < 10 && vX > 0){
			runningMode = true;
			currentAnimation = bpRunningRightAnimation;
			if(inSpeedRoll){
				currentAnimation = bpJumpRollRightAnimation;
			}
		}
		else if(vX < -10){
			runningMode = true;
			currentAnimation = bpRunningFastLeftAnimation;
			if(inSpeedRoll){
				currentAnimation = bpJumpRollLeftAnimation;
			}
		}
		else if(vX > -10 && vX < 0){
			runningMode = true;
			currentAnimation = bpRunningLeftAnimation;
			if(inSpeedRoll){
				currentAnimation = bpJumpRollLeftAnimation;
			}
		}

		if (vX < 0.5 && vX > -0.5) {

			if(rightOrLeft){
				currentAnimation = bpStanceRightAnimation;
			}
			else{
				currentAnimation = bpStanceLeftAnimation;

			}
			vX = 0.0f;
			runningMode = false;
			inSpeedRoll = false;
			return;

		}
		if (vX > 0.0) {
			if(inSpeedRoll){
				vX -=0.1;
			}
			else{
				vX -= 0.3;
			}
		} 
		else {
			if(inSpeedRoll){
				vX +=0.1;
			}
			else{
				vX += 0.3;
			}
		}
	}
	
	/*
	 * all the getters & setters
	 */
	public boolean inAir(){
		return flying;
	}
	public void stance(boolean ask){
		if(flying){
			return;
		}
		else if(vX < 0.5 && vX > -0.5){
			this.rightOrLeft = ask;
		}
		else{
			return;
		}
	}
	public boolean stance(){
		return rightOrLeft;
	}

	public boolean spinChargeBoolean(boolean spin){
		this.inSpin = spin;
		return spin;
	}
	public boolean inSpin(){
		return inSpin;
	}

	public boolean isKeyDown(boolean down){
		this.isDown = down;
		return down;
	}
	public boolean isDown(){
		return isDown;
	}
	public boolean inSpeedRoll(boolean speed){
		this.inSpeedRoll = speed;
		return speed;
	}
	public boolean inSpeedRoll(){
		return inSpeedRoll;
	}
	public boolean runningMode(boolean running){
		this.runningMode = running;
		return runningMode;
	}

	public boolean runningMode(){
		return runningMode;
	}
	public boolean inSpeedLeft(){
		return inSpeedLeftMode;
	}
	public boolean inSpeedRight(){
		return inSpeedRightMode;
	}
	public boolean inTrampoline(){
		return trampoline;
	}


	public float getX(){
		return startPosX;
	}

	public float getY(){
		return startPosY;
	}

	public float getSpeedX(){
		return vX;
	}

	public float getSpeedY(){
		return vY;
	}

	public Shape getShape(){
		return playerCircle;
	}

	public Circle getLeft(){		
		if(inSpeedRoll) return new Circle((int)playerCircle.getCenterX() - 55,(int) playerCircle.getY() + 25, 8, 8);

		Circle left = new Circle((int)playerCircle.getCenterX() - 30,(int) playerCircle.getY() + 25, 8, 8);
		return left;
	}

	public Circle getRight(){
		if(inSpeedRoll) return new Circle((int)playerCircle.getCenterX() + 55,(int) playerCircle.getY() + 25, 8, 8);

		Circle right = new Circle((int)playerCircle.getCenterX() + 30,(int) playerCircle.getY() + 25, 8, 8);
		return right;
	}

	public int getRings(){
		return rings.size();
	}

	public void addRing( Rings ring ){
		rings.add(ring);
	}

	
	public void countRings(){
		if(!done){
				rings.remove(rings.toArray()[rings.size()-1]);
				addPoints(100);
				AudioPlayer.playSound(Reference.SOUND_ENDCOUNTER);
			
		}
		if(rings.size() <= 0){
			done = true;
			ringsCounted = true;
		}
	}
	
	public boolean ringsCounted(){
		return ringsCounted;
	}

	public int getPoints(){
		return points;
	}
	public void addPoints( int points ){
		this.points += points;
	}

	public int getLife(){
		return lives;
	}

	public void setLife( int lives ){
		this.lives = lives;
	}

	public boolean getDying(){
		return dying;
	}

	public boolean getLevelComplete(){
		return levelComplete;
	}
	
	public void setLevelComplete( boolean levelComplete){
		this.levelComplete = levelComplete;
	}

	public void setDying( boolean dying ){
		this.dying = dying;
	}

	public boolean gameOver(){
		return gameOver;
	}

	public void setGameOver( boolean gameOver ){
		this.gameOver = gameOver;
	}

	public void setX(double newX)
	{
		this.startPosX = (float) newX;
	}

	public void setY(double newY)
	{
		this.startPosY = (float) newY;
	}

	public void setSpeedX(double newVX)
	{
		this.vX = (float) newVX;
	}

	public void setSpeedY(double newVY)
	{
		this.vY = (float) newVY;
	}

}


package se.nackademin.joakimrezek.entity;



import org.newdawn.slick.GameContainer;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Shape;
import org.newdawn.slick.state.StateBasedGame;

/*
 * creating the rings
 */

import se.nackademin.joakimrezek.SizedAnimation;

public class Rings {
	
	//instance of coordinates, shape, sprites and animation
	private SpriteSheet ring;
	private SizedAnimation ringAnimation;
	private SizedAnimation currentAnimation;
	
	private int x;
	private int y;
	private Shape rect;

	
	public Rings(int x, int y){ //constructor
		this.x = x;
		this.y = y;
		
	}
	
	/*
	 * initializing all instances
	 */
	public void init(GameContainer gc, StateBasedGame sbg)throws SlickException {
		ring = new SpriteSheet("NonStaticBackground/Rings.png", 30, 32);
		ringAnimation = new SizedAnimation(ring, 100, 64, 64);
		rect = new Rectangle(x + 10, y + 10, 44, 44);
		
		currentAnimation = ringAnimation;
	}
	
	public void update(GameContainer gc, StateBasedGame sbg, int delta)throws SlickException {
		
		
	}
	
	/*
	 * rendering the current animation
	 */
	public void render(GameContainer gc, StateBasedGame sbg, Graphics g)throws SlickException {
		currentAnimation.draw(x, y, currentAnimation.getSizeX(), currentAnimation.getSizeY());
	}
	
	public Shape getShape(){ //returning the shape
		return rect;
	}

}

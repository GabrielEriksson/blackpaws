package se.nackademin.joakimrezek.entity;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Shape;
import org.newdawn.slick.state.StateBasedGame;

/*
 * creating the static spike
 */

public class Spike {
	
	/*
	 * instances of coordinates, direction, images and shape
	 */
	private int x, y;
	private String direction;
	
	private Image spikeLeft;
	private Image spikeRight;
	private Image spikeUp;
	private Image spikeDown;
	
	private Image currentImage;
	
	private Shape rect;
		
	public Spike(int x, int y, String direction){ //constructor
		this.x = x;
		this.y = y;
		this.direction = direction;

	}
	
	/*
	 * initializing all the instances
	 */
	public void init(GameContainer gc, StateBasedGame sbg)throws SlickException {
		spikeLeft = new Image("res/TrapLeft.png");
		spikeRight = new Image("res/TrapRight.png");
		spikeUp = new Image("res/TrapUp.png");
		spikeDown = new Image("res/TrapDown.png");
		
		
		rect = new Rectangle(x, y, 75, 75);
		
	}
	
	/*
	 * checking the direction and setting current image
	 */
	public void update(GameContainer gc, StateBasedGame sbg, int delta)throws SlickException {
		
		if(direction.equals("up")) currentImage = spikeUp;
			if(direction.equals("down")) currentImage = spikeDown;
				if(direction.equals("left")) currentImage = spikeLeft;
					if(direction.equals("right")) currentImage = spikeRight;
		
	}
	
	/*
	 * rendering the current image
	 */
	public void render(GameContainer gc, StateBasedGame sbg, Graphics g)throws SlickException {
		currentImage.draw(x, y, 75, 75);		
	}

	public Shape getShape(){ //returning the shape
		return rect;
	}

}

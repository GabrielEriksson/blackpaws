package se.nackademin.joakimrezek.libs;

/*
 * creating my references
 */
public class Reference {
	
	/*
	 * making the static final strings of locations
	 */
    public static final String SOUND_LOCATION = "./sounds/";

    public static final String SOUND_BOUNCER = "bouncer";
    public static final String SOUND_BREAKS = "breaks";
    public static final String SOUND_DIES = "dies";
    public static final String SOUND_ENDCOUNTER = "endcounter";
    public static final String SOUND_JUMP = "jump";
    public static final String SOUND_JUMPONENEMY = "jumponenemy";
    public static final String SOUND_LEVELCOMPLETE = "levelcomplete";
    public static final String SOUND_LEVELCOMPLETED = "levelcompleted";
    public static final String SOUND_RING = "ring";
    public static final String SOUND_RINGSLOST = "ringslost";
    public static final String SOUND_SPINCHARGE = "spincharge";
    public static final String SOUND_INSPIN = "inspin";
    public static final String SOUND_NAILSTING = "nailsting";
    public static final String SOUND_SPINCHARGERELEASED = "spinchargereleased";
    public static final String SOUND_SPINCHARGERELEASED2 = "spinchargereleased2";
    public static final String SOUND_PRESSSTART = "pressstart";
    public static final String MUSIC_MENUMUSIC = "menumusic";
    public static final String MUSIC_GAMEMUSIC = "gamemusic";
    
    

}

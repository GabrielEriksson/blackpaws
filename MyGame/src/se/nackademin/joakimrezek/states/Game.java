package se.nackademin.joakimrezek.states;

import java.awt.Font;

import javax.swing.Timer;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.TrueTypeFont;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.state.transition.EmptyTransition;
import org.newdawn.slick.state.transition.FadeOutTransition;

import se.nackademin.joakimrezek.Camera;
import se.nackademin.joakimrezek.MyGame;
import se.nackademin.joakimrezek.entity.Player;
import se.nackademin.joakimrezek.libs.Reference;
import se.nackademin.joakimrezek.utils.AudioPlayer;
import se.nackademin.joakimrezek.utils.Time;
import se.nackademin.joakimrezek.world.Introduction;
import se.nackademin.joakimrezek.world.World;


/*
 * creating the game state
 */
public class Game extends BasicGameState{
	
	/*
	 * instances of inputs, player, world, camera, introduction, time and fonts
	 */
	Input input;
	private Player player;
	private World level;
	private Camera camera;
	private Introduction aztekIntro;
	
	private Time time;
	
	private Font font;
	private TrueTypeFont tFont;
	
	private Image livesLeft;
	private Image levelCompleted;

	private boolean started;
	private boolean levelCompletedSoundPlayed;
	private boolean levelComplete;
	private boolean startCounting;
	public static boolean DONE;
	private int stateID;
	
	public Game(int stateID) { //constructor
		super();
		this.stateID = stateID;
	}

	/*
	 * (non-Javadoc)
	 * @see org.newdawn.slick.state.GameState#init(org.newdawn.slick.GameContainer, org.newdawn.slick.state.StateBasedGame)
	 * initializing all the instances
	 */
	
	
	//whenever I enter this state I call its init method to restart
	@Override
	public void enter(GameContainer gc, StateBasedGame sbg) throws SlickException{
		init(gc, sbg);
	}
	
	
	//init method
	@Override
	public void init(GameContainer gc, StateBasedGame sbg) throws SlickException {
		input = new Input(0);
		time = new Time(6);
		camera = new Camera();
		level = new World();
		player = new Player(level, camera);
		level.setPlayer(player);
		time.setPlayer(player);
		time.init(gc, sbg);
		player.init(gc, sbg);
		level.init(gc, sbg);	
		
		startCounting = false;
		levelComplete = false;
		started = false;
		levelCompletedSoundPlayed = false;

		
		aztekIntro = new Introduction(new Image("NonStaticBackground/LevelStartNameY.png"),
										new Image("NonStaticBackground/LevelStartNameB.png"),
										new Image("NonStaticBackground/LevelStartNameR.png"));
		
		
		font = new Font("Comic Sans B", Font.BOLD, 16);
		tFont = new TrueTypeFont(font, false);
		
		livesLeft = new Image("NonStaticBackground/lifeHead.png");
		levelCompleted = new Image("res/LevelComplete.png");
		

		
	}

	/*
	 * (non-Javadoc)
	 * @see org.newdawn.slick.state.GameState#update(org.newdawn.slick.GameContainer, org.newdawn.slick.state.StateBasedGame, int)
	 * calling all the update methods
	 */
	@Override
	public void update(GameContainer gc, StateBasedGame sbg, int delta) throws SlickException {
		if(!started){
			AudioPlayer.playMusic(Reference.MUSIC_GAMEMUSIC);
			started = true;
		}
		
		if(gc.getInput().isKeyPressed(Input.KEY_ESCAPE)){
			MyGame.exit();
		}
		
		time.update(gc, sbg, delta);
		
		player.update(gc, sbg, delta);

		level.update(gc, sbg, delta);
		
		camera.tick(player);
		
		aztekIntro.update(gc, sbg, delta);
		
		
		//if times up I will die and gameover
		if(time.timesUp() && !player.getLevelComplete() && !DONE){
			gameOverTimeLimit();
		}
		
		//if level completed start calculating
		if(player.getLevelComplete() && !DONE){
			endGame();
		}
		
		//games done
		if(player.gameOver()){
			closeGame(sbg);
		}
		
	
		
	}
	
	/*
	 * (non-Javadoc)
	 * @see org.newdawn.slick.state.GameState#render(org.newdawn.slick.GameContainer, org.newdawn.slick.state.StateBasedGame, org.newdawn.slick.Graphics)
	 * calling different methods accordingly to the different inputs
	 */
	@Override
	public void render(GameContainer gc, StateBasedGame sbg, Graphics g) throws SlickException {
		/*
		 * translating movements to all things in between, in other words, if camera moves, so does things in between
		 */
		g.translate(camera.getX(), camera.getY());
		level.renderFarAway(player, camera);
		level.RenderBackground(gc, sbg, g);
		player.render(gc, sbg, g);
		level.render(gc, sbg, g);
		g.translate(-camera.getX(), -camera.getY());

		
		/*
		 * rendering statuses
		 */
		livesLeft.draw(20, 0, 50, 50);
		tFont.drawString(75, 15, "" + player.getLife(), Color.white);
		tFont.drawString(20, 40, "Points: " + player.getPoints(), Color.white);
		tFont.drawString(20, 60, "Rings collected: " + (player.getRings() - 1), Color.white);
		tFont.drawString(20, 80, "Rings left: " + (level.getRings()), Color.white);
		time.render(gc, sbg, g);
		aztekIntro.render(gc, sbg, g);
	
		//LEVEL COMPLETE sign
		if(DONE){
			levelCompleted.draw(0, 0);
		}
		
		
		//all the inputs
		if(gc.getInput().isKeyPressed(Input.KEY_LEFT)){
			player.stance(false);
		}
		if(gc.getInput().isKeyPressed(Input.KEY_RIGHT)){
			player.stance(true);
		}
		if(gc.getInput().isKeyDown(Input.KEY_DOWN) && !player.getDying()){
			if(player.inAir()){
				player.jump();
			}
			if(player.runningMode() && !player.inAir()){
				player.inSpeedRoll(true);
				player.roll();
				player.breakX();
			}
			else{
				if(!player.runningMode() && !player.inSpeedRoll() && !player.inAir()){
					player.isKeyDown(true);
					player.sit();
						if(gc.getInput().isKeyPressed(Input.KEY_SPACE)){
							player.inSpeedRoll(true);
							player.spinChargeBoolean(true);
							player.spinCharge(); 
							AudioPlayer.playSound(Reference.SOUND_SPINCHARGE);
						}	
			}
				else if(player.inSpin() && gc.getInput().isKeyPressed(Input.KEY_SPACE)){
					AudioPlayer.playSound(Reference.SOUND_SPINCHARGE);	
				}
		}
		}

		else if(gc.getInput().isKeyDown(Input.KEY_RIGHT) && !player.getDying()){
			if(player.inSpeedLeft()){
				player.stop();
			}
			player.runningMode(true);
				player.runRight();
			
			if(gc.getInput().isKeyPressed(Input.KEY_SPACE)){
				player.jump();
			}

		}
		else if(gc.getInput().isKeyDown(Input.KEY_LEFT) && !player.getDying()){
			if(player.inSpeedRight()){
				player.stop();
			}
			player.runningMode(true);
				player.runLeft();
			if(gc.getInput().isKeyPressed(Input.KEY_SPACE)){
				player.jump();
			}
		}
		else if (gc.getInput().isKeyPressed(Input.KEY_SPACE)){
			player.jump();
		}
		
		else {
			player.isKeyDown(false);
			player.spinCharge();
			if (!player.inAir()) {
				player.breakX();
			}
		}
	}
	
	
	//time's up
	public void gameOverTimeLimit(){
		player.setLife(0);
		player.dies();
	}
	
	
	//calculating method
	public void endGame(){
			time.stop(true);
			
			if(!startCounting){
			Timer timer = new Timer(3000, e -> startCounting = true);
			timer.setRepeats(false);
			timer.start();
			}
		
			if(startCounting){
			time.countDownAndCalculate();
			player.countRings();
			}
			
		
		
		if(player.getLevelComplete() && time.timeCounted() && (player.ringsCounted())){
			levelComplete = true;
			Timer timer = new Timer(3000, e -> player.setGameOver(true));
			timer.setRepeats(false);
			timer.start();
			
			if(!levelCompletedSoundPlayed){
				AudioPlayer.playSound(Reference.SOUND_LEVELCOMPLETED);
				}
			levelCompletedSoundPlayed = true;
		}
		if(levelComplete){
			levelComplete = false;
		player.setLevelComplete(false);
		player.setGameOver(false);
		startCounting = false;
		DONE = true;
		}

	}
	
	//close the game and change state to game over
	public void closeGame(StateBasedGame sbg)throws SlickException{
		sbg.enterState(3, new FadeOutTransition(Color.black), new EmptyTransition());
		AudioPlayer.playSound(Reference.SOUND_PRESSSTART);
	}
	
	
	//whenever I leave I'm reseting the game so thereby also DONE boolean needs to reset
	//not really sure I need this
	@Override
	public void leave(GameContainer gc, StateBasedGame sbg) throws SlickException{
		DONE = false;
	}
	

	@Override
	public int getID() { //returning game's stateID

		return stateID;
	}

	
}	

	
package se.nackademin.joakimrezek.states;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.state.transition.EmptyTransition;
import org.newdawn.slick.state.transition.FadeOutTransition;

import se.nackademin.joakimrezek.MyGame;
import se.nackademin.joakimrezek.libs.Reference;
import se.nackademin.joakimrezek.utils.AudioPlayer;

/*
 * creating my Game Over state
 */
public class GameOver extends BasicGameState{
	/*
	 * instances of Images, booleans and states
	 */
	private int stateID;

	private Image backgroundRoller;
	private Image title;
	private Image background;
	private Image gameOver;
	
	private boolean started;

	
	public GameOver(int stateID){ //constructor
		this.stateID = stateID;
	}

	
	//init method
	@Override
	public void init(GameContainer gc, StateBasedGame sbg)throws SlickException {
		started = false;

		backgroundRoller = new Image("NonStaticBackground/Menu.png");
		title = new Image("NonStaticBackground/title.png");
		background = new Image("NonStaticBackground/MenuBG.png");
		gameOver = new Image("NonStaticBackground/GameOver.png");
		
	}

	//start menu music, esc to quit, enter to restart
	@Override
	public void update(GameContainer gc, StateBasedGame sbg, int delta)throws SlickException {
		if(!started){
		AudioPlayer.playMusic(Reference.MUSIC_MENUMUSIC);
		started = true;
		}
		
		if(gc.getInput().isKeyPressed(Input.KEY_ESCAPE)){
			MyGame.exit();
		}

		if(gc.getInput().isKeyPressed(Input.KEY_ENTER)){
			sbg.enterState(0, new FadeOutTransition(Color.black), new EmptyTransition());
		}
		
	}
	
	// render all
	@Override
	public void render(GameContainer gc, StateBasedGame sbg, Graphics g)throws SlickException {
		background.draw(0, 100);
		backgroundRoller.draw(0, 0);
		title.draw(50, 0);
		
		gameOver.draw(200, 0);
		
		
	}

	//returning stateID
	@Override
	public int getID() {
		return stateID;
	}
	


}



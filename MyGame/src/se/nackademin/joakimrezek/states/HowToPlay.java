package se.nackademin.joakimrezek.states;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.state.transition.EmptyTransition;
import org.newdawn.slick.state.transition.FadeOutTransition;

import se.nackademin.joakimrezek.MyGame;
import se.nackademin.joakimrezek.libs.Reference;
import se.nackademin.joakimrezek.utils.AudioPlayer;
	/*
	 * creating my How To Play state
	 */
public class HowToPlay extends BasicGameState {
	
	/*
	 * instances of image and states
	 */
	private int stateID;
	private Image howToPlayImage;
	
	public HowToPlay(int stateID){ //constructor
		this.stateID = stateID;
	}
	
	//init method
	public void init(GameContainer gc, StateBasedGame sbg)throws SlickException {
		howToPlayImage = new Image("res/HowToPlay.png");
	}
	
	//enter to start game, if so play start sound and stop music, esc to quit
	public void update(GameContainer gc, StateBasedGame sbg, int delta)throws SlickException {
		if(gc.getInput().isKeyPressed(Input.KEY_ENTER)){
			sbg.enterState(2, new FadeOutTransition(Color.black), new EmptyTransition());
			AudioPlayer.playSound(Reference.SOUND_PRESSSTART);
			AudioPlayer.getMusic(Reference.MUSIC_MENUMUSIC).stop();
		}
		
		if(gc.getInput().isKeyPressed(Input.KEY_ESCAPE)){
			MyGame.exit();
		}
		
	}
	//render image
	public void render(GameContainer gc, StateBasedGame sbg, Graphics g)throws SlickException {
		howToPlayImage.draw(0, 0);
		
	}

	//returning stateID
	@Override
	public int getID() {
		return stateID;
	}
}

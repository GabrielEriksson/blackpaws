package se.nackademin.joakimrezek.states;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.state.transition.EmptyTransition;
import org.newdawn.slick.state.transition.FadeOutTransition;

import se.nackademin.joakimrezek.MyGame;
import se.nackademin.joakimrezek.SizedAnimation;
import se.nackademin.joakimrezek.libs.Reference;
import se.nackademin.joakimrezek.utils.AudioPlayer;

/*
 * creating the menu class
 */
public class Menu extends BasicGameState{
	/*
	 * instances of sprites, animation, images, coordinates and states
	 */
	private int stateID;
	
	private SpriteSheet pressStart;
	private SizedAnimation pressStartAnimation;
	
	private Image backgroundRoller;
	private Image title;
	private Image background;

	
	private int x;
	private int originalX;
	
	private boolean goingRight;
	private boolean started;
	
	public Menu(int stateID){ //constructor initializing stateID
		this.stateID = stateID; 
	}
	
	/*
	 * (non-Javadoc)
	 * @see org.newdawn.slick.state.GameState#init(org.newdawn.slick.GameContainer, org.newdawn.slick.state.StateBasedGame)
	 * initializing all the instances and playing the sounds when prompted
	 */
	@Override
	public void init(GameContainer gc, StateBasedGame sbg)throws SlickException {
		goingRight = false;
		started = false;
		
		pressStart = new SpriteSheet("NonStaticBackground/PressStart.png", 305, 300);
		pressStartAnimation = new SizedAnimation(pressStart, 400, 100, 100);
		
		backgroundRoller = new Image("NonStaticBackground/Menu.png");
		title = new Image("NonStaticBackground/title.png");
		background = new Image("NonStaticBackground/MenuBG.png");
		
	}

	/*
	 * (non-Javadoc)
	 * @see org.newdawn.slick.state.GameState#update(org.newdawn.slick.GameContainer, org.newdawn.slick.state.StateBasedGame, int)
	 * making game to change to gaming state when prompted and playing the sounds which belongs
	 * setting background to move forth and back
	 */
	@Override
	public void update(GameContainer gc, StateBasedGame sbg, int delta)throws SlickException {
		if(!started){
			AudioPlayer.playMusic(Reference.MUSIC_MENUMUSIC);
			started = true;
		}
		
		if(gc.getInput().isKeyPressed(Input.KEY_ESCAPE)){
			MyGame.exit();
		}
		
		if(gc.getInput().isKeyPressed(Input.KEY_ENTER)){
				sbg.enterState(1, new FadeOutTransition(Color.black), new EmptyTransition());
				AudioPlayer.playSound(Reference.SOUND_PRESSSTART);
				
		}
		
		if(!goingRight) x -= 1;
		
		if(goingRight) x += 1;

		if(x == -700) goingRight = true;
		
		if(x == originalX)	goingRight = false;
		
		
		
	}
	
	/*
	 * (non-Javadoc)
	 * @see org.newdawn.slick.state.GameState#render(org.newdawn.slick.GameContainer, org.newdawn.slick.state.StateBasedGame, org.newdawn.slick.Graphics)
	 * rendering menu
	 */
	@Override
	public void render(GameContainer gc, StateBasedGame sbg, Graphics g)throws SlickException {
		background.draw(x, 100);
		backgroundRoller.draw(0, 0);
		title.draw(50, 0);
		pressStartAnimation.draw(495, 400);

	}

	/*
	 * (non-Javadoc)
	 * @see org.newdawn.slick.state.BasicGameState#getID()
	 * returning menu's state ID
	 */
	@Override
	public int getID() {
		return stateID;
	}

}

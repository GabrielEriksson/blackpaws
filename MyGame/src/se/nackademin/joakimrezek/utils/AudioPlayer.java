package se.nackademin.joakimrezek.utils;

import java.util.HashMap;
import java.util.Map;

import org.newdawn.slick.Music;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.Sound;

import se.nackademin.joakimrezek.libs.Reference;

/*
 * creating the Audio Player
 */

public class AudioPlayer {

	//making the hashmaps
    private static Map<String, Sound> soundMap = new HashMap<String, Sound>();
    private static Map<String, Music> musicMap = new HashMap<String, Music>();

    /*
     * method to add sounds
     */
    public static void addSound(String key, String path){
        try {
            soundMap.put(key, new Sound(Reference.SOUND_LOCATION + path));
        } catch (SlickException e) {
            
            e.printStackTrace();
        }
    }

    /*
     * method to add music
     */
    public static void addMusic(String key, String path){
        try {
            musicMap.put(key, new Music(Reference.SOUND_LOCATION + path));
        } catch (SlickException e) {
            
            e.printStackTrace();
        }
    }

    public static Sound getSound(String key){ //returning sound at string position
        return soundMap.get(key);
    }

    public static Music getMusic(String key){ //returning music at string position
        return musicMap.get(key);
    }
    
    public static void playSound(String key){ //playing sound at string position
        soundMap.get(key).play(1f, 0.5f); 
    }
    
    public static void playMusic(String key){ //playing music at string position 
        musicMap.get(key).loop(1f, 0.7f);
    }
    

}
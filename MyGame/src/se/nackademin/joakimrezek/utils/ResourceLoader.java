package se.nackademin.joakimrezek.utils;

import se.nackademin.joakimrezek.libs.Reference;


/*
 * creating my reasource loader
 */
public class ResourceLoader {

	/*
	 * method to load the sounds and music into the hashmaps inside Audio Player
	 */
    public static void loadSounds(){
        AudioPlayer.addSound(Reference.SOUND_BOUNCER, "Bouncer.ogg");
        AudioPlayer.addSound(Reference.SOUND_BREAKS, "Breaks.ogg");
        AudioPlayer.addSound(Reference.SOUND_DIES, "Dies.ogg");
        AudioPlayer.addSound(Reference.SOUND_ENDCOUNTER, "EndCounter.ogg");
        AudioPlayer.addSound(Reference.SOUND_JUMP, "Jump.ogg");
        AudioPlayer.addSound(Reference.SOUND_JUMPONENEMY, "JumpOnEnemy.ogg");
        AudioPlayer.addSound(Reference.SOUND_LEVELCOMPLETE, "LevelComplete.ogg");
        AudioPlayer.addSound(Reference.SOUND_LEVELCOMPLETED, "LevelCompleted.ogg");
        AudioPlayer.addSound(Reference.SOUND_RING, "Ring.ogg");
        AudioPlayer.addSound(Reference.SOUND_RINGSLOST, "RingsLost.ogg");
        AudioPlayer.addSound(Reference.SOUND_SPINCHARGE, "SpinCharge.ogg");
        AudioPlayer.addSound(Reference.SOUND_INSPIN, "InSpin.ogg");
        AudioPlayer.addSound(Reference.SOUND_NAILSTING, "NailSting.ogg");
        AudioPlayer.addSound(Reference.SOUND_SPINCHARGERELEASED, "SpinChargeReleased.ogg");
        AudioPlayer.addSound(Reference.SOUND_SPINCHARGERELEASED2, "SpinChargeReleased2.ogg");
        AudioPlayer.addSound(Reference.SOUND_PRESSSTART, "PressStart.ogg");
        
        
        AudioPlayer.addMusic(Reference.MUSIC_MENUMUSIC, "MenuMusic.ogg");
        AudioPlayer.addMusic(Reference.MUSIC_GAMEMUSIC, "GameMusic.ogg");
    }

}

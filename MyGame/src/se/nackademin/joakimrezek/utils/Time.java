package se.nackademin.joakimrezek.utils;

import java.awt.Font;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.TrueTypeFont;
import org.newdawn.slick.state.StateBasedGame;

import se.nackademin.joakimrezek.entity.Player;
import se.nackademin.joakimrezek.libs.Reference;

/*
 * creating my timer clock in the game
 */
public class Time {
	
	/*
	 * instances of fonts, start time, secs, min, and states
	 */
	private Font font;
	private TrueTypeFont tFont;
	
	private int startTime;
	
	private int seconds;
	private int minutes;
	
	private long milliseconds;
	
	private boolean timesUp;
	private boolean timeStopped;
	private boolean timeCounted;
	
	Player player;

	
	public Time(int startTime){ //constructor
		this.startTime = startTime;
	}
	
	/*
	 * initializing all the instances
	 */
	public void init(GameContainer gc, StateBasedGame sbg)throws SlickException {
		timesUp = false;
		timeStopped = false;
		timeCounted = false;
		
		
		font = new Font("Comic Sans B", Font.BOLD, 16);
		tFont = new TrueTypeFont(font, false);
		
		minutes = startTime;
		seconds = 0;
		milliseconds = 60;	//game is set on 60 fps //therefor dividing all by 60 millisecs to make it count seconds	
	}
	
	/*
	 * setting the countdown to begin
	 */
	public void update(GameContainer gc, StateBasedGame sbg, int delta)throws SlickException {
		
		if(!timeStopped){
		milliseconds--;
		if(!timesUp && milliseconds == 0){
			seconds--;
			milliseconds = 60;
		}
		if(seconds == -1 && minutes != 0){
			seconds = 59;
			minutes--;
		}
		if(minutes == 0 && seconds == 0){
			minutes = 0;
			seconds = 0;
			timesUp = true;
		}
		}
		
	}
	
	/*
	 * rendering the current time
	 */
	public void render(GameContainer gc, StateBasedGame sbg, Graphics g)throws SlickException {
		tFont.drawString(20, 100, "Time: " + minutes + ":" + seconds, Color.white);
		
	}
	
	public boolean timesUp(){
		if(minutes == 0 && seconds == 0){
			return true;
		}
		return false;
	}
	
	public boolean timeCounted(){
		return timeCounted;
	}
	
	public void stop( boolean timeStopped){
		this.timeStopped = timeStopped;
	}
	
	public void countDownAndCalculate(){
		if(!timeCounted){
			player.addPoints(1);
			seconds--;
			
			AudioPlayer.playSound(Reference.SOUND_ENDCOUNTER);
			if(seconds == -1 && minutes != 0){
			seconds = 59;
			minutes--;
			}
			if(minutes == 0 && seconds == 0){
			minutes = 0;
			seconds = 0;
			timeCounted = true;
			}
		}
		
	}
	
	public void setPlayer(Player player){
		this.player = player;
	}

}

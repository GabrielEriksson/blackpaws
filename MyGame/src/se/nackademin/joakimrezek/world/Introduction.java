package se.nackademin.joakimrezek.world;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Timer;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;


/*
 * creating the level introducer
 */
public class Introduction {
	
	/*
	 * instances of images and coordinates
	 */
	private Image yellow;
	private Image blue;
	private Image red;
	
	private int yX, bY, rY;
	
	//constructor
	public Introduction(Image yellow,
						Image blue, 
						Image red){
		this.yellow = yellow;
		this.blue = blue;
		this.red = red;
	}
	
	/*
	 * making timer to decide movement
	 */
	public void update(GameContainer gc, StateBasedGame sbg, int delta) throws SlickException {
		Timer timer1 = new Timer(1000, new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				rY += 10;
				yX -= 10;
				bY -= 10;}});
		
		timer1.start();
	}
	
	/*
	 * rendering the introduction
	 */
	public void render(GameContainer gc, StateBasedGame sbg, Graphics g) throws SlickException {
		blue.draw(0, bY);
		red.draw(0, rY);
		yellow.draw(yX, 0);
	}

}

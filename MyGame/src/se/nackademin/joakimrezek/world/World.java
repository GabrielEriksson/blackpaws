package se.nackademin.joakimrezek.world;

import java.util.ArrayList;

import javax.swing.Timer;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Polygon;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Shape;
import org.newdawn.slick.geom.Transform;
import org.newdawn.slick.state.StateBasedGame;

import se.nackademin.joakimrezek.Camera;
import se.nackademin.joakimrezek.entity.Bat;
import se.nackademin.joakimrezek.entity.Bubbah;
import se.nackademin.joakimrezek.entity.MovingSpike;
import se.nackademin.joakimrezek.entity.Player;
import se.nackademin.joakimrezek.entity.Rings;
import se.nackademin.joakimrezek.entity.Spike;
import se.nackademin.joakimrezek.libs.Reference;
import se.nackademin.joakimrezek.utils.AudioPlayer;

/*
 * creating level 1
 */

public class World {
	
	/*
	 * instances of player, arraylists, shapes, images, spikes, moving spikes, bats, bubbahs and rings
	 */
	Player player;
	
	private static ArrayList<Shape> grounds;
	private static ArrayList<Shape> roofs;
	private static ArrayList<Shape> hillsUpRight;
	private static ArrayList<Shape> hillsUpLeft;
	private static ArrayList<Shape> walls;
	private static ArrayList<Shape> bouncers;
	private static ArrayList<Shape> bouncersRight;
	private static ArrayList<Shape> bouncersLeft;
	private static ArrayList<Shape> bouncersHard;
	private static ArrayList<Shape> fallingPlatforms;
	private static ArrayList<Rings> rings;
	private static ArrayList<Bubbah> bubbahs;
	private static ArrayList<Bat> bats;
	private static ArrayList<MovingSpike> movingSpikes;
	private static ArrayList<Spike> spikes;
	private static ArrayList<Shape> sRight;
	private static ArrayList<Shape> sLeft;


	/*
	 * shapes: grounds, walls, roofs, hills, bouncers, bridges, falling platforms and lifts
	 */
	private Shape ground1, ground2, ground3, ground4, ground5, ground6, ground7, ground8, ground9, ground10, ground11, ground12, ground13, ground14, ground15,
			ground16, ground17, ground18, ground19, ground20, ground21, ground22, ground23, ground24, ground25, ground26, ground27, ground28, ground29, ground30,
			ground31, ground32, ground33, ground34, ground35, ground36, ground37, ground38, ground39, roof1, roof2, roof3, roof4, roof5, roof6, roof7, roof8, roof9, roof10, roof11, roof12, roof13, roof14, roof15, 
			roof16, roof17, roof18, roof19, roof20, roof21, roof22, roof23, roof24, roof25, roof26, wall1, wall2, wall3, wall4, wall5, wall6, wall7, wall8, wall9, wall10, wall11, wall12, wall13, wall14, wall15, 
			wall16, wall17, wall18, wall19, wall20, wall21, wall22, wall23, wall24, wall25, wall26, wall27, wall28, wall29, wall30, 
			wall31, wall32, wall33, wall34, wall35, wall36, wall37, wall38, wall39, wall40, wall41, wall42, wall43, wall44, wall45, wall46, wall47, wall48, wall49, wall50,
			hill1, hill2, hill3, hill4, hill5, hill6, hill7, bouncer1, bouncer2, bouncer3, bouncer4, bouncer5, bouncer6, bouncer7, bouncer8,
			bouncer9, bouncer10, bouncer11, bouncer12, bouncer13, bouncer14, bouncer15, bridge1, bridge2, fall1, fall2, fall3, fall4, fall5, fall6,
			lift1, lift2, lift3, lift4, lift5, lift6, loopPost1, loopPost2, loopPost3, loopPost4, s1, s2, s3, s4, s5, s6, s7, endGame;
	
	/*
	 * Foreground
	 */
	private Image l1, l2, l3, l4, l5, l6, l7, l8, l9, l10, l11, l12, l13, l14, l15,
	 l16, l17, l18, l19, l20, l21, l22, l23, l24, l25, l26, l27, l28, l29, l30,
	 l31, l32, l33, l34, l35, l36, l37, l38, l39, l40, l41, l42, l43, l44, l45,
	 l46, l47, l48, l49, l50, l51, l52, l53, l54, l55, l56, l57, l58, l59, l60,
	 l61, l62, l63, l64, l65, l66, l67, l68, l69, l70, l71, l72, l73, l74, l75,
	 l76, l77, l78, l79, l80, l81, l82, l83, l84, l85, l86;

	/*
	 * Background
	 */
	private Image lb1, lb2, lb3, lb4, lb5, lb6, lb7, lb8, lb9, lb10, lb11, lb12, lb13, lb14, lb15,
	lb16, lb17, lb18, lb19, lb20, lb21, lb22, lb23, lb24, lb25, lb26, lb27, lb28, lb29, lb30,
	lb31, lb32, lb33, lb34, lb35, lb36, lb37, lb38, lb39, lb40, lb41, lb42, lb43, lb44, lb45,
	lb46, lb47, lb48, lb49, lb50, lb51, lb52, lb53, lb54, lb55, lb56, lb57, lb58, lb59, lb60,
	lb61, lb62, lb63, lb64, lb65, lb66, lb67, lb68, lb69, lb70, lb71, lb72, lb73;
	
	/*
	 * far away backgrounds
	 */
	private Image background, backgroundIsland;
	
	/*
	 * bouncer images
	 */
	private Image bBounce1, bBounce2, bBounce3, bBounce4, bBounce5, bBounce6,
				bBounce7, bBounce8, bBounce9, bBounce10, bBounce11, bBounce12, 
				oBounce1, oBounce2, oBounce3;
	/*
	 * lift images
	 */
	private Image lifts1, lifts2, lifts3, lifts4, lifts5, lifts6;
	
	/*
	 * falling platform images
	 */
	private Image falls1, falls2, falls3, falls4, falls5, falls6, bridges1;
	
	/*
	 * toxic water images
	 */
	private Image toxic1, toxic2;
	
	/*
	 * rings
	 */
	private Rings ring1, ring2, ring3, ring4, ring5, ring6, ring7, ring8, ring9, ring10, ring11, ring12, ring13, ring14, ring15, ring16, ring17, ring18, ring19, ring20, ring21, ring22, ring23, ring24, ring25, 
				ring26, ring27, ring28, ring29, ring30, ring31, ring32, ring33, ring34, ring35, ring36, ring37, ring38, ring39, ring40, ring41, ring42, ring43, ring44, ring45, ring46, ring47, ring48, ring49, ring50,
				ring51, ring52, ring53, ring54, ring55, ring56, ring57, ring58, ring59, ring60, ring61, ring62, ring63, ring64, ring65, ring66, ring67, ring68, ring69, ring70, ring71, ring72, ring73, ring74, ring75, 
				ring76, ring77, ring78, ring79, ring80, ring81, ring82, ring83, ring84, ring85, ring86, ring87, ring88, ring89, ring90, ring91, ring92, ring93, ring94, ring95, ring96, ring97, ring98, ring99, ring100, 
				ring101, ring102, ring103, ring104, ring105, ring106, ring107, ring108, ring109, ring110, ring111, ring112, ring113, ring114, ring115, ring116, ring117, ring118, ring119, ring120, ring121, ring122, ring123, ring124, ring125,
				ring126, ring127, ring128, ring129, ring130, ring131, ring132, ring133, ring134, ring135, ring136, ring137, ring138, ring139, ring140, ring141, ring142, ring143, ring144, ring145, ring146, ring147, ring148, ring149, ring150;
	
	/*
	 * spikes and moving spikes
	 */
	private Spike spikes1, spikes2, spikes3, spikes4, spikes5, spikes6, spikes7, spikes8, spikes9;
	private MovingSpike spike1, spike2, spike3, spike4, spike5, spike6;
	
	/*
	 * enemies: bats and bubbahs
	 */
	private Bubbah bubbah1, bubbah2, bubbah3, bubbah4, bubbah5, bubbah6, bubbah7, bubbah8;
	private Bat bat1, bat2, bat3, bat4, bat5, bat6, bat7, bat8, bat9, bat10, bat11, bat12;
	
	//timer states
	private	boolean timeElapsed1; private boolean timeElapsed4;	
	private	boolean timeElapsed2; private boolean timeElapsed5;
	private	boolean timeElapsed3; private boolean timeElapsed6;
	
	private boolean over1;		
	private boolean over2;	private boolean over4;	
	private boolean over3;	private boolean over5;	

	private boolean overLift1;
	private boolean overLift2;
	private boolean overLift3; 
	private boolean overLift4;
	private boolean overLift5;
	
	private boolean upEnd;
	private boolean downLeftEnd;
	private boolean rightEnd;
	
	private float bgX;
	private float bgY;	
	private float bgiX; 
	private float bgiY;
	
	
	public World(){ //constructor
		super();
	}
	
	
	/*
	 * initializing all the instances
	 */
	public void init(GameContainer gc, StateBasedGame sbg) throws SlickException {
		grounds = new ArrayList<Shape>();
		roofs = new ArrayList<Shape>();
		hillsUpRight = new ArrayList<Shape>();
		hillsUpLeft = new ArrayList<Shape>();
		walls = new ArrayList<Shape>();
		bouncers = new ArrayList<Shape>();
		bouncersRight = new ArrayList<Shape>();
		bouncersLeft = new ArrayList<Shape>();
		bouncersHard = new ArrayList<Shape>();
		fallingPlatforms = new ArrayList<Shape>();
		rings = new ArrayList<Rings>();
		bubbahs = new ArrayList<Bubbah>();
		bats = new ArrayList<Bat>();
		movingSpikes = new ArrayList<MovingSpike>();
		spikes = new ArrayList<Spike>();
		sRight = new ArrayList<Shape>();
		sLeft = new ArrayList<Shape>();
		
	loopPost1 = new Rectangle(5100, 180, 5, 30); 	
	loopPost2 = new Rectangle(9748, -890, 5, 30);
	loopPost3 = new Rectangle(10480, -422, 5, 30);
	loopPost4 = new Rectangle(9159, 1838, 5, 30);
	
	ground1 = new Rectangle(0, 700, 913, 14);		ground11 = new Rectangle(914, 1324, 400, 14);	ground21 = new Rectangle(6336, 752, 697, 14);	ground31 = new Rectangle(11117, 2298, 1918, 14);	
	ground2 = new Rectangle(1423, 364, 414, 14);	ground12 = new Rectangle(3428, 2100, 530, 14);	ground22 = new Rectangle(7032, 1843, 1143, 14);	ground32 = new Rectangle(12758, 2098, 1242, 14);
	ground3 = new Rectangle(1998, 124, 573, 14);	ground13 = new Rectangle(3958, 1852, 385, 14);	ground23 = new Rectangle(7440, 1462, 228, 14);	ground33 = new Rectangle(4885, -264, 435, 14);
	ground4 = new Rectangle(2571, -70, 450, 14);	ground14 = new Rectangle(4343, 1575, 609, 14);	ground24 = new Rectangle(7778, 900, 218, 14);	ground34 = new Rectangle(9597, -1334, 435, 14);
	ground5 = new Rectangle(3021, -280, 400, 14);	ground15 = new Rectangle(4952, 1215, 446, 14);	ground25 = new Rectangle(7778, 400, 218, 14);	ground35 = new Rectangle(10307, -867, 435, 14);
	ground6 = new Rectangle(4147, 215, 1300, 14);	ground16 = new Rectangle(5721, 1215, 615, 14);	ground26 = new Rectangle(8177, 276, 276, 14);	ground36 = new Rectangle(9524, 971, 675, 14);
	ground7 = new Rectangle(5317, 215, 497, 14);	ground17 = new Rectangle(2242, 1703, 398, 14);	ground27 = new Rectangle(8454, -1248, 700, 14);	ground37 = new Rectangle(8705, 666, 627, 14);
	ground8 = new Rectangle(6050, 215, 285, 14);	ground18 = new Rectangle(2770, 1703, 397, 14);	ground28 = new Rectangle(10796, 1275, 1274, 14);ground38 = new Rectangle(8947, 1399, 435, 14);
	ground9 = new Rectangle(1837, 2100, 528, 14);	ground19 = new Rectangle(2568, 666, 855, 14);	ground29 = new Rectangle(8947, 1874, 905, 14);	ground39 = new Rectangle(6113, -2130, 224, 14);
	ground10 = new Rectangle(914, 1720, 921, 14);	ground20 = new Rectangle(2787, 954, 637, 14);	ground30 = new Rectangle(8947, 2503, 2595, 14);
	
	grounds.add((Rectangle) ground1);	grounds.add((Rectangle) ground8);	grounds.add((Rectangle) ground15);	grounds.add((Rectangle) ground22);	grounds.add((Rectangle) ground29);	grounds.add((Rectangle) ground36);
	grounds.add((Rectangle) ground2);	grounds.add((Rectangle) ground9);	grounds.add((Rectangle) ground16); 	grounds.add((Rectangle) ground23);	grounds.add((Rectangle) ground30);	grounds.add((Rectangle) ground37);
	grounds.add((Rectangle) ground3);	grounds.add((Rectangle) ground10);	grounds.add((Rectangle) ground17);	grounds.add((Rectangle) ground24);	grounds.add((Rectangle) ground31);	grounds.add((Rectangle) ground38);
	grounds.add((Rectangle) ground4);	grounds.add((Rectangle) ground11);	grounds.add((Rectangle) ground18);	grounds.add((Rectangle) ground25);	grounds.add((Rectangle) ground32);	grounds.add((Rectangle) ground39);
	grounds.add((Rectangle) ground5);	grounds.add((Rectangle) ground12);	grounds.add((Rectangle) ground19);	grounds.add((Rectangle) ground26);	grounds.add((Rectangle) ground33);
	grounds.add((Rectangle) ground6);	grounds.add((Rectangle) ground13);	grounds.add((Rectangle) ground20);	grounds.add((Rectangle) ground27);	grounds.add((Rectangle) ground34);
	grounds.add((Rectangle) ground7);	grounds.add((Rectangle) ground14);	grounds.add((Rectangle) ground21);	grounds.add((Rectangle) ground28);	grounds.add((Rectangle) ground35);
	
	s1 = new Rectangle(6330, 225, 400, 14); s4 = new Rectangle(11480, 195, 400, 14);
	s2 = new Rectangle(6450, 496, 390, 14); s5 = new Rectangle(11600, 530, 480, 14);
	s3 = new Rectangle(6330, 751, 500, 14); s6 = new Rectangle(11480, 910, 400, 14);
										   s7 = new Rectangle(11480, 1275, 600, 14);
										   
	sRight.add(s1); sRight.add(s3); sRight.add(s4); sRight.add(s6);
	sLeft.add(s2); sLeft.add(s5); sLeft.add(s7);
	
	roof1 = new Rectangle(914, 1152, 594, 14);	roof11 = new Rectangle(5158, 980, 657, 14);	roof21 = new Rectangle(9524, 1072, 675, 14);
	roof2 = new Rectangle(914, 1487, 400, 14);	roof12 = new Rectangle(6052, 980, 285, 14);	roof22 = new Rectangle(8705, 762, 627, 14);
	roof3 = new Rectangle(1510, 1487, 327, 14);	roof13 = new Rectangle(6854, 128, 1143, 14);		roof23 = new Rectangle(8947, 2247, 1830, 14);
	roof4 = new Rectangle(2010, 1790, 622, 14);	roof14 = new Rectangle(7268, 1672, 402, 14);	roof24 = new Rectangle(10781, 1890, 1980, 14);
	roof5 = new Rectangle(2243, 1235, 924, 14);	roof15 = new Rectangle(7779, 1672, 220, 14);	roof25 = new Rectangle(6113, -2054, 224, 14);
	roof6 = new Rectangle(2770, 1790, 652, 14);	roof16 = new Rectangle(7560, 1342, 218, 14); roof26 = new Rectangle(7440, 740, 320, 14);
	roof7 = new Rectangle(2571, 317, 601, 14);	roof17 = new Rectangle(7440, 717, 555, 14);
	roof8 = new Rectangle(3171, -85, 130, 14);	roof18 = new Rectangle(8460, 395, 485, 14);
	roof9 = new Rectangle(3302, 317, 1856, 14);	roof19 = new Rectangle(8945, 230, 197, 14);
	roof10 = new Rectangle(2787, 786, 637, 14);	roof20 = new Rectangle(9142, 395, 2330, 14);
	
	roofs.add(roof1);	roofs.add(roof8);	roofs.add(roof15);	roofs.add(roof22);
	roofs.add(roof2);	roofs.add(roof9);	roofs.add(roof16);	roofs.add(roof23);
	roofs.add(roof3);	roofs.add(roof10);	roofs.add(roof17);	roofs.add(roof24);
	roofs.add(roof4);	roofs.add(roof11);	roofs.add(roof18);	roofs.add(roof25);
	roofs.add(roof5);	roofs.add(roof12);	roofs.add(roof19);	roofs.add(roof26);
	roofs.add(roof6);	roofs.add(roof13);	roofs.add(roof20);
	roofs.add(roof7);	roofs.add(roof14);	roofs.add(roof21);
	
	wall1 = new Rectangle(860, 1160, 60, 630);	wall11 = new Rectangle(2770, 1706, 60, 96);	wall21 = new Rectangle(4945, 1218, 60, 400);	wall31 = new Rectangle(7935, 401, 60, 330);		wall41 = new Rectangle(8948, 2509, 60, 281);
	wall2 = new Rectangle(1508, 1120, 60, 377);	wall12 = new Rectangle(3172, 1270, 60, 520);	wall22 = new Rectangle(5162, 292, 60, 700);	wall32 = new Rectangle(8177, 282, 282, 2650);	wall42 = new Rectangle(7610, 1467, 60, 215);
	wall3 = new Rectangle(1255, 1325, 60, 173);	wall13 = new Rectangle(2512, 300, 60, 400);	wall23 = new Rectangle(5752, 219, 60, 775);	wall33 = new Rectangle(8455, -1240, 491, 1649);	wall43 = new Rectangle(6336, 1027, 60, 230);
	wall4 = new Rectangle(1775, 368, 60, 1130);	wall14 = new Rectangle(3110, -98, 60, 424);	wall24 = new Rectangle(6052, 219, 60, 775);	wall34 = new Rectangle(11476, 84, 1283, -3133);	wall44 = new Rectangle(6840, 110, 6, 580);
	wall5 = new Rectangle(1775, 1726, 60, 400);	wall15 = new Rectangle(3295, -98, 60, 424);wall25 = new Rectangle(6337, 145 , 1660,-3183);	wall35 = new Rectangle(11468, 320, 6, 880);	wall45 = new Rectangle(3429, 2107, 60, 700);	
	wall6 = new Rectangle(2008, 129, 60, 1676);	wall16 = new Rectangle(3361, 667, 60, 138);	wall26 = new Rectangle(6110,-2128, 60, 80);	wall36 = new Rectangle(9143, 230, 60, 178);		wall46 = new Rectangle(2302, 2107, 60, 700);
	wall7 = new Rectangle(2572, -68, 60, 220);	wall17 = new Rectangle(2729, 800, 60, 178);	wall27 = new Rectangle(6969, 755, 60, 1190);	wall37 = new Rectangle(8706, 670, 626, 104);	wall47 = new Rectangle(6277, 226, 60, 700);
	wall8 = new Rectangle(3021, -278, 60, 220);	wall18 = new Rectangle(3361, 961, 60, 840);	wall28 = new Rectangle(7270, 755, 185, 918);wall38 = new Rectangle(9525, 975, 674, 108);	wall48 = new Rectangle(12046, 1245, 60, -1300);
	wall9 = new Rectangle(2185, 1270, 60, 520); wall19 = new Rectangle(3959, 1858, 60, 300);	wall29 = new Rectangle(7560, 1050, 218, 300);wall39 = new Rectangle(8948, 1880, 1831, 380); wall49 = new Rectangle(11119, 2500, 60, -195);
	wall10 = new Rectangle(2572, 1706, 60, 96);	wall20 = new Rectangle(4344, 1580, 60, 300);	wall30 = new Rectangle(7779, 908, 220, 770);wall40 = new Rectangle(12702, 1899, 60, -1930);	wall50 = new Rectangle(12757, 2300, 60, -195);
	
	walls.add(wall1);	walls.add(wall8);	walls.add(wall15);	walls.add(wall22);	walls.add(wall29);	walls.add(wall36);	walls.add(wall43);	walls.add(wall50);
	walls.add(wall2);	walls.add(wall9);	walls.add(wall16);	walls.add(wall23);	walls.add(wall30);	walls.add(wall37);	walls.add(wall44);
	walls.add(wall3);	walls.add(wall10);	walls.add(wall17);	walls.add(wall24);	walls.add(wall31);	walls.add(wall38);	walls.add(wall45);
	walls.add(wall4);	walls.add(wall11);	walls.add(wall18);	walls.add(wall25);	walls.add(wall32);	walls.add(wall39);	walls.add(wall46);
	walls.add(wall5);	walls.add(wall12);	walls.add(wall19);	walls.add(wall26);	walls.add(wall33);	walls.add(wall40);	walls.add(wall47);
	walls.add(wall6);	walls.add(wall13);	walls.add(wall20);	walls.add(wall27);	walls.add(wall34);	walls.add(wall41);	walls.add(wall48);
	walls.add(wall7);	walls.add(wall14);	walls.add(wall21);	walls.add(wall28);	walls.add(wall35);	walls.add(wall42);	walls.add(wall49);
	
	float[] hill1Points = new float[] {913, 700, 1423, 364, 1423, 364, 1423, 700, 1000, 700, 913, 700};				hill1 = new Polygon(hill1Points);						
	float[] hill2Points = new float[] {3421, -280, 4147, 215, 4147, 215, 3421, 215, 3421, 215, 3421, -280};			hill2 = new Polygon(hill2Points);
	float[] hill3Points = new float[] {7778, 900, 7778, 1053, 7778, 1053, 7557, 1053, 7557, 1053, 7778, 900};		hill3 = new Polygon(hill3Points);
	float[] hill4Points = new float[] {7778, 400, 7778, 752, 7778, 752, 7270, 752, 7270, 752, 7778, 400};			hill4 = new Polygon(hill4Points);
	float[] hill5Points = new float[] {9154, -1248, 11170, 79, 11170, 79, 9154, 79,	9145, 79, 9154, -1248};			hill5 = new Polygon(hill5Points);		
	float[] hill6Points = new float[] {11170, 79, 11470, 185, 11470, 185, 11170, 185, 11170, 185, 11170, 79};		hill6 = new Polygon(hill6Points);				
	float[] hill7Points = new float[] {10796, 1275, 10796, 1874,10796, 1874, 9850, 1874, 9850, 1874, 10796, 1275};	hill7 = new Polygon(hill7Points);						
	
	hillsUpRight.add(hill1);	hillsUpLeft.add(hill2);	hillsUpRight.add(hill3);	hillsUpRight.add(hill4);	hillsUpLeft.add(hill5);	hillsUpLeft.add(hill6);	hillsUpRight.add(hill7);

	endGame = new Rectangle(13680, 1900, 4, 200);
	
	bouncer1 = new Rectangle(1836, 2050, 100, 40);	bouncer3 = new Rectangle(4830, 1540, 100, 40);	
	bouncer2 = new Rectangle(3185, 630, 100, 40);	bouncer4 = new Rectangle(6228, -2150, 100, 40);

	bouncer5 = new Rectangle(3088,1260,100,40).transform(Transform.createRotateTransform((float)Math.toRadians(45), 5700, 4000));
	bouncer6 = new Rectangle(4327,-600,100,40).transform(Transform.createRotateTransform((float)Math.toRadians(45), 5700, 4000));
	bouncer7 = new Rectangle(3887,-1044,100,40).transform(Transform.createRotateTransform((float)Math.toRadians(45), 5700, 4000));
	bouncer8 = new Rectangle(3397,-1538,100,40).transform(Transform.createRotateTransform((float)Math.toRadians(45), 5700, 4000));
	
	bouncer9 = new Rectangle(200,200,100,40).transform(Transform.createRotateTransform((float)Math.toRadians(135), 4110, 2120));
	bouncer10 = new Rectangle(-350,400,100,40).transform(Transform.createRotateTransform((float)Math.toRadians(135), 4110, 2120));
	bouncer11 = new Rectangle(-850,900,100,40).transform(Transform.createRotateTransform((float)Math.toRadians(135), 4110, 2120));
	bouncer12 = new Rectangle(-1315,1350,100,40).transform(Transform.createRotateTransform((float)Math.toRadians(135), 4110, 2120));
	
	bouncer13 = new Rectangle(2500,2190,100,40).transform(Transform.createRotateTransform((float)Math.toRadians(135), 4110, 2120));
	bouncer14 = new Rectangle(-800,3850,100,40).transform(Transform.createRotateTransform((float)Math.toRadians(135), 4110, 2120));
	
	bouncer15 = new Rectangle(6300, 1100, 40, 100);
	
	bouncers.add(bouncer1);	bouncers.add(bouncer2);	bouncers.add(bouncer3);	bouncers.add(bouncer4);
	bouncersRight.add(bouncer5); bouncersRight.add(bouncer6); bouncersRight.add(bouncer7); bouncersRight.add(bouncer8);
	bouncersLeft.add(bouncer9); bouncersLeft.add(bouncer10); bouncersLeft.add(bouncer11); bouncersLeft.add(bouncer12);
	bouncersHard.add(bouncer13); bouncersHard.add(bouncer14);
	
	bridge1 = new Rectangle(5400,1215,322,4);
	bridge2 = new Rectangle(7027,752,250,4);
	grounds.add(bridge1); fallingPlatforms.add(bridge2);
	
	fall1 = new Rectangle(7023,1620, 120, 40); 	fall6 = new Rectangle(8700, 2500, 240, 40);	
	fall2 = new Rectangle(7160, 1330, 120, 40);	
	fall3 = new Rectangle(7023, 1032, 120, 40);	
	fall4 = new Rectangle(8770, 2220, 180, 40);	
	fall5 = new Rectangle(8460, 2500, 240, 40);	
	fallingPlatforms.add(fall1);	fallingPlatforms.add(fall6);	
	fallingPlatforms.add(fall2);	
	fallingPlatforms.add(fall3);		
	fallingPlatforms.add(fall4);		
	fallingPlatforms.add(fall5);	
	
	lift1 = new Rectangle(3758, -280, 190, 40);		lift4 = new Rectangle(3430, 1830, 190, 40);
	lift2 = new Rectangle(5860, -1150, 190, 40);	lift5 = new Rectangle(2368, 2113, 190, 40);
	lift3 = new Rectangle(5840, 1170, 190, 40);		lift6 = new Rectangle(5668, -950, 190, 40);

	l1 = new Image("levelPics/3.0.png");	l16 = new Image("levelPics/4.0.png");	l31 = new Image("levelPics/5.0.png");	l46 = new Image("levelPics/5.15.png");	l61 = new Image("levelPics/6.14.png");	l76 = new Image("levelPics/1.9.png");	
	l2 = new Image("levelPics/3.1.png");	l17 = new Image("levelPics/4.1.png");	l32 = new Image("levelPics/5.1.png");	l47 = new Image("levelPics/6.0.png");	l62 = new Image("levelPics/6.15.png");	l77 = new Image("levelPics/1.10.png");	
	l3 = new Image("levelPics/3.2.png");	l18 = new Image("levelPics/4.2.png");	l33 = new Image("levelPics/5.2.png");	l48 = new Image("levelPics/6.1.png");	l63 = new Image("levelPics/2.4.png");	l78 = new Image("levelPics/1.12.png");	
	l4 = new Image("levelPics/3.3.png");	l19 = new Image("levelPics/4.3.png");	l34 = new Image("levelPics/5.3.png");	l49 = new Image("levelPics/6.2.png");	l64 = new Image("levelPics/2.5.png");	l79 = new Image("levelPics/1.13.png");	
	l5 = new Image("levelPics/3.4.png");	l20 = new Image("levelPics/4.4.png");	l35 = new Image("levelPics/5.4.png");	l50 = new Image("levelPics/6.3.png");	l65 = new Image("levelPics/2.6.png");	l80 = new Image("levelPics/1.14.png");	
	l6 = new Image("levelPics/3.5.png");	l21 = new Image("levelPics/4.5.png");	l36 = new Image("levelPics/5.5.png");	l51 = new Image("levelPics/6.4.png");	l66 = new Image("levelPics/2.8.png");	l81 = new Image("levelPics/0.6.png");	
	l7 = new Image("levelPics/3.6.png");	l22 = new Image("levelPics/4.6.png");	l37 = new Image("levelPics/5.6.png");	l52 = new Image("levelPics/6.5.png");	l67 = new Image("levelPics/2.8b.png");	l82 = new Image("levelPics/0.7.png");	
	l8 = new Image("levelPics/3.7.png");	l23 = new Image("levelPics/4.7.png");	l38 = new Image("levelPics/5.7.png");	l53 = new Image("levelPics/6.6.png");	l68 = new Image("levelPics/2.9.png");	l83 = new Image("levelPics/0.8.png");	
	l9 = new Image("levelPics/3.8.png");	l24 = new Image("levelPics/4.8.png");	l39 = new Image("levelPics/5.8.png");	l54 = new Image("levelPics/6.7.png");	l69 = new Image("levelPics/2.10.png");	l84 = new Image("levelPics/0.12.png");	
	l10 = new Image("levelPics/3.9.png");	l25 = new Image("levelPics/4.9.png");	l40 = new Image("levelPics/5.9.png");	l55 = new Image("levelPics/6.8.png");	l70 = new Image("levelPics/2.11.png");	l85 = new Image("levelPics/0.13.png");	
	l11 = new Image("levelPics/3.10.png");	l26 = new Image("levelPics/4.10.png");	l41 = new Image("levelPics/5.10.png");	l56 = new Image("levelPics/6.9.png");	l71 = new Image("levelPics/2.12.png");	l86 = new Image("levelPics/0.14.png");	
	l12 = new Image("levelPics/3.11.png");	l27 = new Image("levelPics/4.11.png");	l42 = new Image("levelPics/5.11.png");	l57 = new Image("levelPics/6.10.png");	l72 = new Image("levelPics/2.13.png");	
	l13 = new Image("levelPics/3.12.png");	l28 = new Image("levelPics/4.12.png");	l43 = new Image("levelPics/5.12.png");	l58 = new Image("levelPics/6.11.png");	l73 = new Image("levelPics/2.14.png");	
	l14 = new Image("levelPics/3.13.png");	l29 = new Image("levelPics/4.13.png");	l44 = new Image("levelPics/5.13.png");	l59 = new Image("levelPics/6.12.png");	l74 = new Image("levelPics/1.7.png");	
	l15 = new Image("levelPics/3.14.png");	l30 = new Image("levelPics/4.14.png");	l45 = new Image("levelPics/5.14.png");	l60 = new Image("levelPics/6.13.png");	l75 = new Image("levelPics/1.8.png");	
	
	lb1 = new Image("levelPicsB/1.1.png");	lb16 = new Image("levelPicsB/2.4.png");	lb31 = new Image("levelPicsB/3.6.png");	lb46 = new Image("levelPicsB/4.6.png");	lb61 = new Image("levelPicsB/5.7.png");	
	lb2 = new Image("levelPicsB/1.2.png");	lb17 = new Image("levelPicsB/2.5.png");	lb32 = new Image("levelPicsB/3.7.png");	lb47 = new Image("levelPicsB/4.7.png");	lb62 = new Image("levelPicsB/5.8.png");	
	lb3 = new Image("levelPicsB/1.3.png");	lb18 = new Image("levelPicsB/2.6.png");	lb33 = new Image("levelPicsB/3.8.png");	lb48 = new Image("levelPicsB/4.8.png");	lb63 = new Image("levelPicsB/5.9.png");		
	lb4 = new Image("levelPicsB/1.4.png");	lb19 = new Image("levelPicsB/2.7.png");	lb34 = new Image("levelPicsB/3.9.png");	lb49 = new Image("levelPicsB/4.9.png");	lb64 = new Image("levelPicsB/5.10.png");		
	lb5 = new Image("levelPicsB/1.5.png");	lb20 = new Image("levelPicsB/2.8.png");	lb35 = new Image("levelPicsB/3.10.png");	lb50 = new Image("levelPicsB/4.10.png");	lb65 = new Image("levelPicsB/5.11.png");		
	lb6 = new Image("levelPicsB/1.6.png");	lb21 = new Image("levelPicsB/2.9.png");	lb36 = new Image("levelPicsB/3.11.png");	lb51 = new Image("levelPicsB/4.11.png");	lb66 = new Image("levelPicsB/5.12.png");		
	lb7 = new Image("levelPicsB/1.7.png");	lb22 = new Image("levelPicsB/2.10.png");	lb37 = new Image("levelPicsB/3.12.png");	lb52 = new Image("levelPicsB/4.12.png");	lb67 = new Image("levelPicsB/5.13.png");		
	lb8 = new Image("levelPicsB/1.8.png");	lb23 = new Image("levelPicsB/2.11.png");	lb38 = new Image("levelPicsB/3.13.png");	lb53 = new Image("levelPicsB/4.13.png");	lb68 = new Image("levelPicsB/5.14.png");		
	lb9 = new Image("levelPicsB/1.9.png");	lb24 = new Image("levelPicsB/2.12.png");	lb39 = new Image("levelPicsB/3.14.png");	lb54 = new Image("levelPicsB/4.14.png");	lb69 = new Image("levelPicsB/5.15.png");	
	lb10 = new Image("levelPicsB/1.10.png");	lb25 = new Image("levelPicsB/3.0.png");	lb40 = new Image("levelPicsB/3.15.png");	lb55 = new Image("levelPicsB/4.15.png");	lb70 = new Image("levelPicsB/6.2.png");
	lb11 = new Image("levelPicsB/1.11.png");	lb26 = new Image("levelPicsB/3.1.png");	lb41 = new Image("levelPicsB/4.1.png");	lb56 = new Image("levelPicsB/5.1.png");	lb71 = new Image("levelPicsB/6.3.png");	
	lb12 = new Image("levelPicsB/2.0.png");	lb27 = new Image("levelPicsB/3.2.png");	lb42 = new Image("levelPicsB/4.2.png");	lb57 = new Image("levelPicsB/5.2.png");	lb72 = new Image("levelPicsB/6.6.png");	
	lb13 = new Image("levelPicsB/2.1.png");	lb28 = new Image("levelPicsB/3.3.png");	lb43 = new Image("levelPicsB/4.3.png");	lb58 = new Image("levelPicsB/5.3.png");	lb73 = new Image("levelPicsB/6.9.png");	
	lb14 = new Image("levelPicsB/2.2.png");	lb29 = new Image("levelPicsB/3.4.png");	lb44 = new Image("levelPicsB/4.4.png");	lb59 = new Image("levelPicsB/5.4.png");		
	lb15 = new Image("levelPicsB/2.3.png");	lb30 = new Image("levelPicsB/3.5.png");	lb45 = new Image("levelPicsB/4.5.png");	lb60 = new Image("levelPicsB/5.6.png");	
	
	background = new Image("NonStaticBackground/Background.png");
	backgroundIsland = new Image("NonStaticBackground/BackgroundIsland.png");
	bBounce1 = new Image("NonStaticBackground/BlueBounceU.png"); oBounce1 = new Image("NonStaticBackground/OrangeBounceLU.png");	falls4 = new Image("NonStaticBackground/FallingBridge.png");
	bBounce2 = new Image("NonStaticBackground/BlueBounceU.png"); oBounce2 = new Image("NonStaticBackground/OrangeBounceLU.png");	falls5 = new Image("NonStaticBackground/FallingBridge.png");
	bBounce3 = new Image("NonStaticBackground/BlueBounceRU.png");oBounce3 = new Image("NonStaticBackground/OrangeBounceU.png");		falls6 = new Image("NonStaticBackground/FallingBridge.png");
	bBounce4 = new Image("NonStaticBackground/BlueBounceU.png");	lifts1 = new Image("NonStaticBackground/Lift.png");				bridges1 = new Image("NonStaticBackground/FallingBridge.png");
	bBounce5 = new Image("NonStaticBackground/BlueBounceU.png");	lifts2 = new Image("NonStaticBackground/Lift.png");
	bBounce6 = new Image("NonStaticBackground/BlueBounceLU.png");	lifts3 = new Image("NonStaticBackground/Lift.png");
	bBounce7 = new Image("NonStaticBackground/BlueBounceLU.png");	lifts4 = new Image("NonStaticBackground/Lift.png");
	bBounce8 = new Image("NonStaticBackground/BlueBounceLU.png");	lifts5 = new Image("NonStaticBackground/Lift.png");
	bBounce9 = new Image("NonStaticBackground/BlueBounceLU.png");	lifts6 = new Image("NonStaticBackground/Lift.png");
	bBounce10 = new Image("NonStaticBackground/BlueBounceRU.png");	falls1 = new Image("NonStaticBackground/FallingBridge.png");
	bBounce11 = new Image("NonStaticBackground/BlueBounceRU.png");	falls2 = new Image("NonStaticBackground/FallingBridge.png");
	bBounce12 = new Image("NonStaticBackground/BlueBounceRU.png");	falls3 = new Image("NonStaticBackground/FallingBridge.png");
	
	toxic1 = new Image("NonStaticBackground/Water.png");
	toxic2 = new Image("NonStaticBackground/Water.png");
	
	ring1 = new Rings(500, 400); rings.add(ring1);	ring11 = new Rings(1900, 100); rings.add(ring11); ring21 = new Rings(975, 1650); rings.add(ring21); ring31 = new Rings(2500, 1550); rings.add(ring31); ring41 = new Rings(2975, 1550); rings.add(ring41);
	ring2 = new Rings(550, 400); rings.add(ring2);	ring12 = new Rings(1900, 200); rings.add(ring12); ring22 = new Rings(1025, 1650); rings.add(ring22); ring32 = new Rings(2325, 1475); rings.add(ring32); ring42 = new Rings(3025, 1550); rings.add(ring42);
	ring3 = new Rings(600, 400); rings.add(ring3);	ring13 = new Rings(1900, 300); rings.add(ring13); ring23 = new Rings(1000, 1600); rings.add(ring23); ring33 = new Rings(2375, 1475); rings.add(ring33); ring43 = new Rings(3075, 1550); rings.add(ring43);
	ring4 = new Rings(650, 400); rings.add(ring4);	ring14 = new Rings(1900, 400); rings.add(ring14); ring24 = new Rings(975, 1250); rings.add(ring24); ring34 = new Rings(2425, 1475); rings.add(ring34); ring44 = new Rings(2900, 1475); rings.add(ring44);
	ring5 = new Rings(700, 400); rings.add(ring5);	ring15 = new Rings(1900, 500); rings.add(ring15); ring25 = new Rings(1025, 1250); rings.add(ring25); ring35 = new Rings(2475, 1475); rings.add(ring35); ring45 = new Rings(2950, 1475); rings.add(ring45);
	ring6 = new Rings(525, 350); rings.add(ring6);	ring16 = new Rings(1900, 600); rings.add(ring16); ring26 = new Rings(1075, 1250); rings.add(ring26); ring36 = new Rings(2350, 1400); rings.add(ring36); ring46 = new Rings(3000, 1475); rings.add(ring46);
	ring7 = new Rings(575, 350); rings.add(ring7);	ring17 = new Rings(1900, 700); rings.add(ring17); ring27 = new Rings(2300, 1550); rings.add(ring27); ring37 = new Rings(2400, 1400); rings.add(ring37); ring47 = new Rings(3050, 1475); rings.add(ring47);
	ring8 = new Rings(625, 350); rings.add(ring8);	ring18 = new Rings(1900, 800); rings.add(ring18); ring28 = new Rings(2350, 1550); rings.add(ring28); ring38 = new Rings(2450, 1400); rings.add(ring38); ring48 = new Rings(2925, 1400); rings.add(ring48);
	ring9 = new Rings(675, 350); rings.add(ring9);	ring19 = new Rings(1900, 900); rings.add(ring19); ring29 = new Rings(2400, 1550); rings.add(ring29); ring39 = new Rings(2875, 1550); rings.add(ring39); ring49 = new Rings(2975, 1400); rings.add(ring49);
	ring10 = new Rings(725, 350); rings.add(ring10);ring20 = new Rings(1900, 1000); rings.add(ring20); ring30 = new Rings(2450, 1550); rings.add(ring30); ring40 = new Rings(2925, 1550); rings.add(ring40); ring50 = new Rings(3025, 1400); rings.add(ring50);
	
	ring51 = new Rings(3400, 860); rings.add(ring51);	ring61 = new Rings(6200, 1080); rings.add(ring61); ring71 = new Rings(5140, 145); rings.add(ring71); ring81 = new Rings(7120, 1350); rings.add(ring81); ring91 = new Rings(9675, -1400); rings.add(ring91);
	ring52 = new Rings(3350, 860); rings.add(ring52);	ring62 = new Rings(6250, -2375); rings.add(ring62); ring72 = new Rings(5160, -155); rings.add(ring72); ring82 = new Rings(7120, 1550); rings.add(ring82); ring92 = new Rings(9725, -1400); rings.add(ring92);
	ring53 = new Rings(3300, 860); rings.add(ring53);	ring63 = new Rings(6250, -2425); rings.add(ring63); ring73 = new Rings(5215, 85); rings.add(ring73); ring83 = new Rings(7120, 1150); rings.add(ring83); ring93 = new Rings(9775, -1400); rings.add(ring93);
	ring54 = new Rings(3250, 860); rings.add(ring54);	ring64 = new Rings(6250, -2475); rings.add(ring64); ring74 = new Rings(5225, -85); rings.add(ring74); ring84 = new Rings(7120, 900); rings.add(ring84); ring94 = new Rings(9825, -1400); rings.add(ring94);
	ring55 = new Rings(3200, 860); rings.add(ring55);	ring65 = new Rings(4900, 0); rings.add(ring65); ring75 = new Rings(5250, 0); rings.add(ring75); ring85 = new Rings(3100, -520); rings.add(ring85); ring95 = new Rings(9875, -1400); rings.add(ring95);
	ring56 = new Rings(3200, 50); rings.add(ring56);	ring66 = new Rings(4935, 85); rings.add(ring66); ring76 = new Rings(5025, 800); rings.add(ring76); ring86 = new Rings(3150, -520); rings.add(ring86); ring96 = new Rings(10400, -930); rings.add(ring96);
	ring57 = new Rings(3200, 100); rings.add(ring57);	ring67 = new Rings(4925, -85); rings.add(ring67); ring77 = new Rings(4750, 700); rings.add(ring77); ring87 = new Rings(3200, -520); rings.add(ring87); ring97 = new Rings(10450, -930); rings.add(ring97);
	ring58 = new Rings(3200, 150); rings.add(ring58);	ring68 = new Rings(5010, 145); rings.add(ring68); ring78 = new Rings(4475, 620); rings.add(ring78); ring88 = new Rings(3250, -520); rings.add(ring88); ring98 = new Rings(10500, -930); rings.add(ring98);
	ring59 = new Rings(6175, 1130); rings.add(ring59);	ring69 = new Rings(4990, -155); rings.add(ring69); ring79 = new Rings(4100, 540); rings.add(ring79); ring89 = new Rings(3300, -520); rings.add(ring89); ring99 = new Rings(10550, -930); rings.add(ring99);
	ring60 = new Rings(6225, 1130); rings.add(ring60);	ring70 = new Rings(5075, -180); rings.add(ring70); ring80 = new Rings(7120, 1700); rings.add(ring80); ring90 = new Rings(750, 400); rings.add(ring90); ring100 = new Rings(10600, -930); rings.add(ring100);
	
	ring101 = new Rings(10850, 1200); rings.add(ring101);	ring111 = new Rings(9600, 900); rings.add(ring111); ring121 = new Rings(8780, 595); rings.add(ring121); ring131 = new Rings(8950, 260); rings.add(ring131); ring141 = new Rings(9280, 1580); rings.add(ring141);
	ring102 = new Rings(10900, 1200); rings.add(ring102);	ring112 = new Rings(9650, 900); rings.add(ring112); ring122 = new Rings(8830, 595); rings.add(ring122); ring132 = new Rings(8950, 320); rings.add(ring132); ring142 = new Rings(8980, 1580); rings.add(ring142);
	ring103 = new Rings(10950, 1200); rings.add(ring103);	ring113 = new Rings(9700, 900); rings.add(ring113); ring123 = new Rings(8880, 595); rings.add(ring123); ring133 = new Rings(9070, 260); rings.add(ring133); ring143 = new Rings(9035, 1520); rings.add(ring143);
	ring104 = new Rings(11000, 1200); rings.add(ring104);	ring114 = new Rings(9750, 900); rings.add(ring114); ring124 = new Rings(8930, 595); rings.add(ring124); ring134 = new Rings(9070, 320); rings.add(ring134); ring144 = new Rings(9230, 1520); rings.add(ring144);
	ring105 = new Rings(11050, 1200); rings.add(ring105);	ring115 = new Rings(9800, 900); rings.add(ring115); ring125 = new Rings(8980, 595); rings.add(ring125); ring135 = new Rings(9220, 1800); rings.add(ring135); ring145 = new Rings(9135, 1480); rings.add(ring145);
	ring106 = new Rings(11100, 1200); rings.add(ring106);	ring116 = new Rings(9850, 900); rings.add(ring116); ring126 = new Rings(9030, 595); rings.add(ring126); ring136 = new Rings(9035, 1800); rings.add(ring136); ring146 = new Rings(8600, 2450); rings.add(ring146);
	ring107 = new Rings(11150, 1200); rings.add(ring107);	ring117 = new Rings(9900, 900); rings.add(ring117); ring127 = new Rings(9080, 595); rings.add(ring127); ring137 = new Rings(9280, 1735); rings.add(ring137); ring147 = new Rings(8650, 2450); rings.add(ring147);
	ring108 = new Rings(11200, 1200); rings.add(ring108);	ring118 = new Rings(9950, 900); rings.add(ring118); ring128 = new Rings(9130, 595); rings.add(ring128); ring138 = new Rings(8980, 1735); rings.add(ring138); ring148 = new Rings(8700, 2450); rings.add(ring148);
	ring109 = new Rings(11250, 1200); rings.add(ring109);	ring119 = new Rings(10000, 900); rings.add(ring119); ring129 = new Rings(9180, 595); rings.add(ring129); ring139 = new Rings(9300, 1660); rings.add(ring139); ring149 = new Rings(10800, 2000); rings.add(ring149);
	ring110 = new Rings(11300, 1200); rings.add(ring110);	ring120 = new Rings(10050, 9000); rings.add(ring120); ring130 = new Rings(9230, 595); rings.add(ring130); ring140 = new Rings(8955, 1660); rings.add(ring140); ring150 = new Rings(10800, 2080); rings.add(ring150);
	
	spikes1 = new Spike(2570, 590, "right"); spikes.add(spikes1);	spikes7 = new Spike(4270, 1640, "left"); spikes.add(spikes7);
	spikes2 = new Spike(3417, 1600, "right"); spikes.add(spikes2);	spikes8 = new Spike(8465, 2000, "right"); spikes.add(spikes8);
	spikes3 = new Spike(3417, 1100, "right"); spikes.add(spikes3); 	spikes9 = new Spike(10780, 1920, "right"); spikes.add(spikes9);
	spikes4 = new Spike(3890, 2000, "left"); spikes.add(spikes4);
	spikes5 = new Spike(3890, 1900, "left"); spikes.add(spikes5);
	spikes6 = new Spike(4270, 1740, "left"); spikes.add(spikes6);
	
	spike1 = new MovingSpike(7287, 1730, true, "up"); movingSpikes.add(spike1); spike5 = new MovingSpike(9485, 2420, true, "up"); movingSpikes.add(spike5);
	spike2 = new MovingSpike(7487, 1730, true, "up"); movingSpikes.add(spike2); spike6 = new MovingSpike(10525, 2420, true, "up"); movingSpikes.add(spike6);
	spike3 = new MovingSpike(7687, 1730, true, "up"); movingSpikes.add(spike3);
	spike4 = new MovingSpike(7887, 1730, true, "up"); movingSpikes.add(spike4);

	bubbah1 = new Bubbah(4775, -480, 400); bubbahs.add(bubbah1); bubbah5 = new Bubbah(9480, 747, 500); bubbahs.add(bubbah5);
	bubbah2 = new Bubbah(2480, -290, 360); bubbahs.add(bubbah2); bubbah6 = new Bubbah(8922, 1180, 290); bubbahs.add(bubbah6);
	bubbah3 = new Bubbah(8450, -1477, 500); bubbahs.add(bubbah3); bubbah7 = new Bubbah(11380, 2075, 1000); bubbahs.add(bubbah7);
	bubbah4 = new Bubbah(10200, -1090, 400); bubbahs.add(bubbah4); bubbah8 = new Bubbah(4323, 1350, 300); bubbahs.add(bubbah8);
	
	bat1 = new Bat(960, 1500, player); bats.add(bat1); bat7 = new Bat(7450, 180, player); bats.add(bat7);
	bat2 = new Bat(1380, 1200, player);bats.add(bat2); bat8 = new Bat(11000, 430, player); bats.add(bat8);
	bat3 = new Bat(2200, 1280, player);bats.add(bat3); bat9 = new Bat(9900, 430, player); bats.add(bat9);
	bat4 = new Bat(2600, 350, player); bats.add(bat4); bat10 = new Bat(8500, 430, player); bats.add(bat10);
	bat5 = new Bat(4600, 350, player); bats.add(bat5); bat11 = new Bat(10820, 1900, player); bats.add(bat11);
	bat6 = new Bat(6250, 1020, player); bats.add(bat6);bat12 = new Bat(10100, 1090, player); bats.add(bat12);
	
	
	timeElapsed1 = false;	timeElapsed2 = false;	timeElapsed3 = false;	timeElapsed4 = false;	timeElapsed5 = false;	timeElapsed6 = false;
	
	over1 = false;		
	over2 = false;	over4 = false;	
	over3 = false;	over5 = false;	
	
	overLift1 = false; overLift2 = false; overLift3 = false; overLift4 = false; overLift5 = false;
	
	upEnd = false;
	downLeftEnd = true;
	rightEnd = false;
	
	bgX = -200; bgY = 0; bgiX = 4000; bgiY = 950;
	/*
	 * calling all init methods from other classes
	 */
	for(Rings R : rings) R.init(gc, sbg);
	for(Bat B : bats) B.init(gc, sbg);
	for(Bubbah B : bubbahs) B.init(gc, sbg);
	for(Spike S : spikes) S.init(gc, sbg);
	for(MovingSpike MS : movingSpikes) MS.init(gc, sbg);
	}

	
	/*
	 * setting lifts to move, timer state to change falling platform to fall
	 */
	public void update(GameContainer gc, StateBasedGame sbg, int delta) throws SlickException {
		moveSideWays(lift1, 3558); 		moveSideWays(lift5, 2368);		moveDiagonal(lift6, 5668, -950);
		moveVertical(lift2, -1150);		moveVertical(lift3, 1170);		moveVertical(lift4, 1830);
		
		if(timeElapsed1) fall1.setY(fall1.getY() + 15);	
		if(timeElapsed2) fall2.setY(fall2.getY() + 15);
		if(timeElapsed3) fall3.setY(fall3.getY() + 15);
		if(timeElapsed4) fall4.setY(fall4.getY() + 15);
		if(timeElapsed5) fall5.setY(fall5.getY() + 15);
		if(timeElapsed6) fall6.setY(fall6.getY() + 15);
		
		/*
		 * calling all the update methods from other classes
		 */
		for(Bubbah B : bubbahs) B.update(gc, sbg, delta);
		for(Bat B : bats) B.update(gc, sbg, delta);
		for(MovingSpike MS : movingSpikes) MS.update(gc, sbg, delta);
		for(Spike S : spikes) S.update(gc, sbg, delta);
		
		
		
	
	}	
	

	/*
	 * rendering the foregrounds
	 */
	public void render(GameContainer gc, StateBasedGame sbg, Graphics g) throws SlickException {	
	for(Rings R : rings) R.render(gc, sbg, g);	
	for(Bubbah B : bubbahs) B.render(gc, sbg, g);
	for(Bat B : bats) B.render(gc, sbg, g);
	for(MovingSpike S : movingSpikes) S.render(gc, sbg, g);
	for(Spike S : spikes) S.render(gc, sbg, g);
		
	
	bBounce1.draw(1845, 2030);	bBounce2.draw(4840, 1485); bBounce3.draw(5769, 220); bBounce4.draw(6233, -2195);
	bBounce5.draw(3189, 585);	bBounce6.draw(8135, 678); bBounce7.draw(8380, 160); bBounce8.draw(8380, -550);
	bBounce9.draw(8380, -1200); bBounce10.draw(7960,-220); bBounce11.draw(7960,-850); bBounce12.draw(7960,-1547);
	oBounce1.draw(6260, -2605); oBounce2.draw(5102, 900); oBounce3.draw(6260, 1100);
	
	lifts1.draw(lift1.getX() - 20, lift1.getY() - 20); lifts4.draw(lift4.getX() - 20, lift4.getY() - 20);
	lifts2.draw(lift2.getX() - 20, lift2.getY() - 20); lifts5.draw(lift5.getX() - 20, lift5.getY() - 20);
	lifts3.draw(lift3.getX() - 20, lift3.getY() - 20); lifts6.draw(lift6.getX() - 20, lift6.getY() - 20);
	
	bridges1.draw(7023, 746);

	falls1.draw(fall1.getX(), fall1.getY(), 125, 100);
	falls2.draw(fall2.getX(), fall2.getY(), 125, 100);
	falls3.draw(fall3.getX(), fall3.getY(), 125, 100);
	falls4.draw(fall4.getX(), fall4.getY());
	falls5.draw(fall5.getX(), fall5.getY());
	falls6.draw(fall6.getX(), fall6.getY());
	
	toxic1.draw(2360, 2080, 1100, 1000);
	toxic2.draw(5390, 1200, 1100, 1300);
		
	l1.draw(0, -120); l2.draw(900, -120); l3.draw(1800, -120); l4.draw(2700, -120); l5.draw(3600, -120); l6.draw(4500, -120); l7.draw(5400, -120); l8.draw(6300, -120); l9.draw(7200, -120); l10.draw(8100, -120); l11.draw(9000, -120); l12.draw(9900, -120); l13.draw(10800, -120); l14.draw(11700, -120); l15.draw(12600, -120); 
	l16.draw(0, 780); l17.draw(900, 780); l18.draw(1800, 780); l19.draw(2700, 780); l20.draw(3600, 780); l21.draw(4500, 780); l22.draw(5400, 780); l23.draw(6300, 780); l24.draw(7200, 780); l25.draw(8100, 780); l26.draw(9000, 780); l27.draw(9900, 780); l28.draw(10800, 780); l29.draw(11700, 780); l30.draw(12600, 780);
	l31.draw(0, 1680); l32.draw(900, 1680); l33.draw(1800, 1680); l34.draw(2700, 1680); l35.draw(3600, 1680); l36.draw(4500, 1680); l37.draw(5400, 1680); l38.draw(6300, 1680); l39.draw(7200, 1680); l40.draw(8100, 1680); l41.draw(9000, 1680); l42.draw(9900, 1680); l43.draw(10800, 1680); l44.draw(11700, 1680); l45.draw(12600, 1680); l46.draw(13500, 1680);
	l47.draw(0, 2580); l48.draw(900, 2580); l49.draw(1800, 2580); l50.draw(2700, 2580); l51.draw(3600, 2580); l52.draw(4500, 2580); l53.draw(5400, 2580); l54.draw(6300, 2580); l55.draw(7200, 2580); l56.draw(8100, 2580); l57.draw(9000, 2580); l58.draw(9900, 2580); l59.draw(10800, 2580); l60.draw(11700, 2580); l61.draw(12600, 2580); l62.draw(13500, 2580);
	l63.draw(2700, -1020); l64.draw(3600, -1020); l65.draw(4500, -1020); l66.draw(6300, -1020); l67.draw(7200, -1020); l68.draw(8100, -1020); l69.draw(9000, -1020); l70.draw(9900, -1020); l71.draw(10800, -1020); l72.draw(11700, -1020); l73.draw(12600, -1020); l74.draw(6300, -1920); l75.draw(7200, -1920); l76.draw(8100, -1920); l77.draw(9000, -1920); l78.draw(10800, -1920);
	l79.draw(11700, -1920); l80.draw(12600, -1920); l81.draw(5400, -2820); l82.draw(6300, -2820); l83.draw(7200, -2820); l84.draw(10800, -2820); l85.draw(11700, -2820); l86.draw(12600, -2820);

}
	
	/*
	 * rendering the backgrounds
	 */
	public void RenderBackground(GameContainer gc, StateBasedGame sbg, Graphics g) throws SlickException {
		lb1.draw(900, -1915); lb2.draw(1800, -1915); lb3.draw(2700, -1915); lb4.draw(3600, -1915); lb5.draw(4500, -1915); lb6.draw(5400, -1915); lb7.draw(6300, -1915); lb8.draw(7200, -1915); lb9.draw(8100, -1915); lb10.draw(9000, -1915); lb11.draw(9900, -1915); 
		lb12.draw(0, -1016); lb13.draw(900, -1016); lb14.draw(1800, -1016); lb15.draw(2700, -1016); lb16.draw(3600, -1016); lb17.draw(4500, -1016); lb18.draw(5400, -1016); lb19.draw(6300, -1016); lb20.draw(7200, -1016); lb21.draw(8100, -1016); lb22.draw(9000, -1016); lb23.draw(9900, -1016); lb24.draw(10800, -1016); 
		lb25.draw(0, -117); lb26.draw(900, -117); lb27.draw(1800, -117); lb28.draw(2700, -117); lb29.draw(3600, -117); lb30.draw(4500, -117); lb31.draw(5400, -117); lb32.draw(6300, -117); lb33.draw(7200, -117); lb34.draw(8100, -117); lb35.draw(9000, -117); lb36.draw(9900, -117); lb37.draw(10800, -117); lb38.draw(11700, -117); lb39.draw(12600, -117); lb40.draw(13500, -117); 
		lb41.draw(900, 782); lb42.draw(1800, 782); lb43.draw(2700, 782); lb44.draw(3600, 782); lb45.draw(4500, 782); lb46.draw(5400, 782);lb47.draw(6300, 782); lb48.draw(7200, 782); lb49.draw(8100, 782); lb50.draw(9000, 782); lb51.draw(9900, 782); lb52.draw(10800, 782); lb53.draw(11700, 782); lb54.draw(12600, 782); lb55.draw(13500, 782); 
		lb56.draw(900, 1681); lb57.draw(1800, 1681); lb58.draw(2700, 1681); lb59.draw(3600, 1681); lb60.draw(5400, 1681); lb61.draw(6300, 1681); lb62.draw(7200, 1681);lb63.draw(8100, 1681); lb64.draw(9000, 1681); lb65.draw(9900, 1681); lb66.draw(10800, 1681); lb67.draw(11700, 1681); lb68.draw(12600, 1681); lb69.draw(13500, 1681); 
		lb70.draw(1800, 2580); lb71.draw(2700, 2580); lb72.draw(5400, 2580);  lb73.draw(8100, 2580); 	
	}
	
	/*
	 * rendering the far away backgrounds
	 */
	public void renderFarAway(Player player, Camera camera){
		if(player.getSpeedX() > 0){
			bgX -= (player.getSpeedX()/1000);
		}
		else if(player.getSpeedX() < 0){
			bgX += (player.getSpeedX()/1000);
		}
		background.draw((float)(bgX - camera.getX()*0.99), (float)(bgY - camera.getY()), 3306, 1300);
		
		
		if(player.getSpeedX() > 0 && player.getX() > 3800 && player.getX() < 5000 && player.getY() > 800) bgiX += 0.2;
		else if(player.getSpeedX() < 0 && player.getX() > 3800 && player.getX() < 5000 && player.getY() > 800) bgiX -= 0.2;
		
		backgroundIsland.draw(bgiX, bgiY, 700, 380);
		
	}

	/*
	 * method to see if player collides with grounds
	 */
	public boolean collidesWithGround( Shape s ){
		for(int i = 0; i < grounds.size(); i++){
		if(s.intersects(grounds.get(i)))
				return true;
		}
		return false;
	}
	
	public boolean collidesWithSRight( Shape s ){
		for(int i = 0; i < sRight.size(); i++){	
			if(s.intersects(sRight.get(i)))
			return true;
		}
		return false;
	}
	
	public boolean collidesWithSLeft( Shape s ){
		for(int i = 0; i < sLeft.size(); i++){
			if(s.intersects(sLeft.get(i)))
			return true;
		}
	return false;
}
	
	/*
	 * method to see if player collides with right uphills
	 */
	public boolean collidesWithHillRight( Shape s ){
		for(int i = 0; i < hillsUpRight.size(); i++){
		if (s.intersects(hillsUpRight.get(i))){
			return true;
			}
		}
		return false;
	}
	
	/*
	 * method to see if player collides with left uphills
	 */
	public boolean collidesWithHillLeft( Shape s ){
		for(int i = 0; i < hillsUpLeft.size(); i++){
		if (s.intersects(hillsUpLeft.get(i))){
			return true;
			}
		}
		return false;
	}
	
	/*
	 * method to see if player collides with roofs
	 */
	public boolean collidesWithRoof( Shape s ){
		for(int i = 0; i < roofs.size(); i++){
		if(s.intersects(roofs.get(i)))
			return true;
		}
		return false;
	}
	
	/*
	 * method to see if player collides with walls
	 */
	public boolean collidesWithWall( Shape s ){
		for(int i = 0; i < walls.size(); i++){
			if(s.intersects(walls.get(i)))
			return true;
		}
		return false;
	}
	
	/*
	 * method to see if player collides with bouncers regular (blue)
	 */
	public boolean collidesWithBouncer( Shape s ){
		for(int i = 0; i < bouncers.size(); i++){
			if(s.intersects(bouncers.get(i)))
				return true;
		}
		return false;
	}
	
	/*
	 * method to see if player collides with bouncers that bounce player to the right (blue)
	 */
	public boolean collidesWithBounceR( Shape s ){
		for(int i = 0; i < bouncersRight.size(); i++){
			if(s.intersects(bouncersRight.get(i)))
					return true;
		}
		return false;
	}
	
	/*
	 * method to see if player collides with bouncers that bounce player to the left (blue)
	 */
	public boolean collidesWithBounceL( Shape s ){
		for(int i = 0; i < bouncersLeft.size(); i++){
			if(s.intersects(bouncersLeft.get(i)))
				return true;
		}
		return false;
	}
	
	/*
	 * method to see if player collides with bouncer that bounce harder up and left (orange)
	 */
	public boolean collidesWithBounceHard( Shape s ){
		for(int i = 0; i < bouncersHard.size(); i++){
			if(s.intersects(bouncersHard.get(i)))
				return true;
		}
		return false;
	}
	
	/*
	 * method to see if player collides with bouncer that bounce harder straight to the left (orange)
	 */
	public boolean collidesWithBounceHardL( Shape s ){
		if(s.intersects(bouncer15))
			return true;
		
		return false;
	}
	
	/*
	 * method to make sure falling platforms fall
	 * states to make sure player don't collides with platforms, unless over the falling platform
	 */
	
	public boolean collidesWithFallingObjects( Shape s ){
		Timer timer1 = new Timer(1000, e -> timeElapsed1 = true);
		Timer timer2 = new Timer(1000, e -> timeElapsed2 = true);
		Timer timer3 = new Timer(1000, e -> timeElapsed3 = true);
		Timer timer4 = new Timer(1000, e -> timeElapsed4 = true);
		Timer timer5 = new Timer(1000, e -> timeElapsed5 = true);
		Timer timer6 = new Timer(1000, e -> timeElapsed6 = true);


		if((s.getY()) > fall1.getY()) over1 = false;
		if((s.getY() + 30) < fall1.getY() - 40) over1 = true;
		if(over1 && s.intersects(fall1) && !timeElapsed1){
			timer1.start();
			return true;
		}
		
		if((s.getY()) > fall2.getY()) over2 = false;
		if((s.getY() + 30) < fall2.getY() - 40) over2 = true;
		if(over2 && s.intersects(fall2) && !timeElapsed2){
			timer2.start();
			return true;
		}
		
		if((s.getY()) > fall3.getY()) over3 = false;
		if((s.getY() + 30) < fall3.getY() - 40) over3 = true;
		if(over3 && s.intersects(fall3) && !timeElapsed3){
			timer3.start();
			return true;
		}
		
		if((s.getY()) > bridge2.getY()) over4 = false;
		if((s.getY() + 30) < bridge2.getY() - 40) over4 = true;
		if(over4 && s.intersects(bridge2)) return true;
		
		if((s.getY()) > fall4.getY()) over5 = false;
		if((s.getY() + 30) < fall4.getY() - 40) over5 = true;
		if(over5 && s.intersects(fall4) && !timeElapsed4){
			timer4.start();
			return true;
		}

		if(s.intersects(fall5) && !timeElapsed5){
			timer5.start();
			return true;
		}

		if(s.intersects(fall6) && !timeElapsed6){
			timer6.start();
			return true;
		}
		
		/*
		 * removing the platforms when below screen
		 */
		if(timeElapsed1 && s.intersects(fallingPlatforms.get(0))){
			if(fall1.getY() > 2700) fallingPlatforms.remove(0); 
		}
		if(timeElapsed2 && s.intersects(fallingPlatforms.get(1))){
			if(fall2.getY() > 2700) fallingPlatforms.remove(1);
		}
		if(timeElapsed3 && s.intersects(fallingPlatforms.get(2))){
			if(fall3.getY() > 2700)fallingPlatforms.remove(2);
		}
		if(timeElapsed4 && s.intersects(fallingPlatforms.get(3))){
			if(fall4.getY() > 2700)fallingPlatforms.remove(3);
		}
		if(timeElapsed5 && s.intersects(fallingPlatforms.get(4))){
			if(fall5.getY() > 2700)fallingPlatforms.remove(4);
		}
		if(timeElapsed6 && s.intersects(fallingPlatforms.get(5))){
			if(fall6.getY() > 2700)fallingPlatforms.remove(5);
		}
		
		return false;
	}

	/*
	 * method to see collision on player and lift
	 * state to make sure player don't collides with lift, unless over the lifts
	 */
	public boolean collidesWithSideLift( Shape s ){
		if((s.getY()) > lift1.getY()) overLift1 = false;
		if((s.getY() + 30) < lift1.getY() - 40) overLift1 = true;
		if(overLift1 && s.intersects(lift1)){
			return true;
		}
		if(s.intersects(lift5)){
			return true;
		}
		
		
		return false;
	}
		
	public boolean collidesWithVerticalLift( Shape s ){
		if((s.getY()) > lift2.getY()) overLift2 = false;
		if((s.getY() + 30) < lift2.getY() - 40) overLift2 = true;
		if(overLift2 && s.intersects(lift2)){
			return true;
		}
		if((s.getY()) > lift3.getY()) overLift3 = false;
		if((s.getY() + 30) < lift3.getY() - 40) overLift3 = true;
		if(overLift3 && s.intersects(lift3)){
			return true;
		}
		if((s.getY()) > lift4.getY()) overLift4 = false;
		if((s.getY() + 30) < lift4.getY() - 40) overLift4 = true;
		if(overLift4 && s.intersects(lift4)){
			return true;
		}
		return false;
	}
		
	public boolean collidesWithDiagonalLift( Shape s ){
		if((s.getY()) > lift6.getY()) overLift5 = false;
		if((s.getY() + 30) < lift6.getY() - 40) overLift5 = true;
		if(overLift5 && s.intersects(lift6)){
			return true;
		}
		return false;
	}
	
	/*
	 * method to see collision of endShape (if I'll complete the game)
	 */
	public boolean collidesWithEnd( Shape s ){
		if(s.intersects(endGame)) return true;
		
		return false;
	}
	
	
	/*
	 * methods to make the lifts move
	 * states to make sure it turns the other way
	 */
	public void moveSideWays( Shape s, float originalPos){
		if(s.getX() == originalPos) rightEnd = false;
		
		if(!rightEnd){
			s.setX((int)s.getX() + 1);
		}
		if(s.getX() == (originalPos + 970)) rightEnd = true;
		
		if (rightEnd){
			s.setX((int)s.getX() - 1);
		}
	}
	
	public void moveVertical( Shape s, float originalPos ){
		if(s.getY() == originalPos) upEnd = false;
		
		if(!upEnd){
			s.setY((int)s.getY() - 1);
		}
		if(s.getY() == (originalPos - 1000)) upEnd = true;
		
		if (upEnd){
			s.setY((int)s.getY() + 1);
		}
	}
	
	public void moveDiagonal( Shape s, float originalPosX, float originalPosY ){
		if(s.getX() == originalPosX) downLeftEnd = false;
		
		if(!downLeftEnd){
			s.setY(s.getY() + 1);
			s.setX((float) (s.getX() - 0.5));
		}
		if(s.getX() == (originalPosX - 300)) downLeftEnd = true;
		
		if (downLeftEnd){
			s.setY((int)s.getY() - 1);
			s.setX((float) (s.getX() + 0.5));
		}
	}
	
	public boolean getInLoop( Shape s ){
		return s.intersects(loopPost1) ? true : false;
	}
	public boolean getInLoop2( Shape s ){
		return s.intersects(loopPost2) ? true : false;
	}
	public boolean getInLoop3( Shape s ){
		return s.intersects(loopPost3) ? true : false;
	}
	public boolean getInLoop4( Shape s ){
		return s.intersects(loopPost4) ? true : false;
	}

	
	/*
	 * method to see collision on rings and play the ring sound
	 * if so, removing it from the list and return it to player
	 */
	public Rings collidesWithRing( Shape s ){
		for(int i = 0; i < rings.size(); i++){
		if(s.intersects(rings.get(i).getShape())){
			AudioPlayer.playSound(Reference.SOUND_RING);
			return rings.remove(i);
			}
		}
		return null;
	}
	
	/*
	 * method to see collision on spike
	 */
	public boolean collidesWithSpike( Shape s ){
		for(int i = 0; i < spikes.size(); i++){
			if(s.intersects(spikes.get(i).getShape())){
				return true;
			}
		}
		return false;
	}
	
	/*
	 * method to see collision on the moving spikes
	 */
	public boolean collidesWithMovingSpike( Shape s ){
		for(int i = 0; i < movingSpikes.size(); i++){
			if(s.intersects(movingSpikes.get(i).getShape())){
				return true;
			}
		}
		return false;
	}
	
	/*
	 * method to see collision with the bubbahs
	 * if so and the right state, remove the bubbah from the list
	 */
	public boolean collidesWithBubbah( Shape s ) throws SlickException{
		for(int i = 0; i < bubbahs.size(); i++){
			if(s.intersects(bubbahs.get(i).getAttackShape())){
				return true;
			}
			if(s.intersects(bubbahs.get(i).getShape()) && player.inAir()){
				bubbahs.remove(i).destroySelf();
				return true;
			}	
		}
		return false;
	}
	
	/*
	 * method to see collision with the bats
	 * if so and in the right state, remove the bat
	 */
	public boolean collidesWithBat( Shape s ) throws SlickException{
		for(int i = 0; i < bats.size(); i++){
			if(s.intersects(bats.get(i).getShape()) && player.inAir()){
				bats.remove(i).destroySelf();
				return true;
			}
			
			if(s.intersects(bats.get(i).getShape()) && !(player.inAir())){
				return true;
			}
		}
		return false;
	}
	
	/*
	 * method to see collision with bubbah's vision shape
	 * if so, calling the bubbah's attack method
	 */
	public void bubbahSeesMe( Shape s ){
		for(int i = 0; i < bubbahs.size(); i++){
			if(s.intersects(bubbahs.get(i).getVisionShape())){
				bubbahs.get(i).attack(true);
			}
			else
				bubbahs.get(i).attack(false);
		}
	}
	
	/*
	 * method to see collision with bat's vision shape
	 * if so, calling the bat's attack method
	 */
	public void batSeesMe( Shape s ){
		for(int i = 0; i < bats.size(); i++){
			if(s.intersects(bats.get(i).getVisionShape())){
				bats.get(i).attack(true);
			}
		}
	}
	
	public int getRings(){ //returning the rings quantity
		return rings.size();
	}
	
	public boolean getRightEnd(){ //returning lifts state on course
		return rightEnd;
	}
	
	public boolean getUpEnd(){ //returning lifts state on course
		return upEnd;
	}
	
	public boolean getDownLeftEnd(){ //returning lifts state on course
		return downLeftEnd;
	}

	public void setPlayer(Player player){ //setting the player
		this.player = player;
	}
	
	
	/*
	 * method for the loop, calculates new coordinates and setting players position
	 */
	public static int placePlayerInLoop(double loopOrigoX, double loopOrigoY, int radius, Player p, int currentStep, boolean backwards) {
		double speed = 30;
		double circ = radius * 2 * Math.PI;
		double stepCnt = circ / Math.abs(speed);
		double angStep = 360 / stepCnt;

		
		double newX = loopOrigoX + Math.cos(Math.toRadians(angStep * (backwards ? stepCnt - currentStep : currentStep) - 90)) * radius;
		double newY = loopOrigoY - Math.sin(Math.toRadians(angStep * (backwards ? stepCnt - currentStep : currentStep) - 90)) * radius;
		
		p.setY(newY);
		p.setX(newX);
		
		if (!backwards)
		{
			return currentStep <= (int) (stepCnt) ? 0 : 1;
		}
		else
		{
			return currentStep <= (int) (stepCnt) ? 0 : -1;
		}
	}
	
}
	
	
